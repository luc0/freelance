-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-03-2016 a las 16:43:02
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `jerbo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campo`
--

CREATE TABLE IF NOT EXISTS `campo` (
  `id` smallint(5) unsigned NOT NULL,
  `id_gestor` smallint(5) unsigned DEFAULT NULL,
  `gestores_contenido` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `opciones` varchar(255) DEFAULT NULL,
  `default` varchar(255) DEFAULT NULL,
  `visible_en_listado` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `html` varchar(255) NOT NULL,
  `orden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `campo`
--

INSERT INTO `campo` (`id`, `id_gestor`, `gestores_contenido`, `nombre`, `tipo`, `opciones`, `default`, `visible_en_listado`, `html`, `orden`, `created_at`, `updated_at`) VALUES
(7, 3, '', 'nombre', 'text', NULL, NULL, 1, 'h2', 0, '2015-12-22 14:01:12', '2015-12-22 14:01:12'),
(9, 3, '', 'src', 'file', NULL, NULL, 1, 'link', 0, '2015-12-22 14:01:49', '2015-12-22 14:01:49'),
(11, 4, '', 'nombre', 'text', NULL, NULL, 1, 'h2', 0, '2015-12-22 22:19:17', '2015-12-22 22:19:17'),
(12, 4, '', 'src', 'file', NULL, NULL, 1, 'link', 0, '2015-12-22 22:19:17', '2015-12-22 22:19:17'),
(14, 5, '', 'nombre', 'text', NULL, NULL, 1, 'h2', 0, '2015-12-22 22:20:11', '2015-12-22 22:20:11'),
(15, 5, '', 'src', 'file', NULL, NULL, 1, 'link', 0, '2015-12-22 22:20:11', '2015-12-22 22:20:11'),
(16, NULL, 'nota', 'titulo', 'text', NULL, NULL, 1, 'h2', 1, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(18, 4, '', 'descargas', 'file', NULL, NULL, 1, 'link', 0, '2015-12-22 22:19:17', '2015-12-22 22:19:17'),
(19, NULL, 'nota', 'copete', 'text', NULL, NULL, 0, 'h3', 0, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(20, NULL, 'nota', 'texto', 'textarea', NULL, NULL, 1, 'p', 4, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(21, NULL, 'nota', 'introduccion', 'textarea', NULL, NULL, 0, 'p', 2, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(22, NULL, 'nota', 'epigrafe', 'textarea', NULL, NULL, 0, 'p', 3, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(23, NULL, 'nota', 'video', 'text', NULL, NULL, 1, 'youtube', 5, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(24, NULL, 'nota', 'imagenes', 'file', NULL, NULL, 1, 'img', 6, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(25, NULL, 'nota', 'audios', 'file', NULL, NULL, 1, 'audio', 7, '2016-01-27 21:13:31', '2016-01-27 21:13:31'),
(26, NULL, 'nota', 'documentos', 'file', NULL, NULL, 1, 'link', 8, '2016-01-27 21:13:31', '2016-01-27 21:13:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id` tinyint(3) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'propósitos', '2015-12-11 17:25:32', '2015-12-11 17:25:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido_audio`
--

CREATE TABLE IF NOT EXISTS `contenido_audio` (
  `id` smallint(5) unsigned NOT NULL,
  `id_subcategoria` tinyint(3) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `src` varchar(255) NOT NULL,
  `activo` tinyint(1) unsigned NOT NULL,
  `orden` smallint(5) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido_galeria_de_fotos`
--

CREATE TABLE IF NOT EXISTS `contenido_galeria_de_fotos` (
  `id` smallint(5) unsigned NOT NULL,
  `id_subcategoria` tinyint(3) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `src` text NOT NULL,
  `descargas` text NOT NULL,
  `activo` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `orden` smallint(5) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido_nota`
--

CREATE TABLE IF NOT EXISTS `contenido_nota` (
  `id` smallint(5) unsigned NOT NULL,
  `id_subcategoria` tinyint(3) unsigned NOT NULL,
  `id_gestor` smallint(5) unsigned NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `formato` tinyint(3) unsigned NOT NULL,
  `copete` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `introduccion` text NOT NULL,
  `epigrafe` text NOT NULL,
  `video` varchar(255) NOT NULL,
  `imagenes` text NOT NULL,
  `audios` text NOT NULL,
  `documentos` text NOT NULL,
  `activo` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `orden` smallint(5) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contenido_nota`
--

INSERT INTO `contenido_nota` (`id`, `id_subcategoria`, `id_gestor`, `titulo`, `formato`, `copete`, `texto`, `introduccion`, `epigrafe`, `video`, `imagenes`, `audios`, `documentos`, `activo`, `orden`, `created_at`, `updated_at`) VALUES
(2, 21, 11, 'Titulo test', 3, '', '   <p>El <strong>Profesor</strong> Dr. Daniel <strong>Stamboulian</strong> se graduó con Honores de la Universidad de Buenos Aires  en 1962, y completó sus estudios de Postgrado en Infectología en la Universidad de Southern California en 1971.  Es fundador y Vice-Presidente de la Asociación Panamericana de Infectología (API) y sirvió como  Presidente desde 1984 a 1986.  Es también miembro fundador de la Sociedad Argentina de Infectología (SADI), y fue el primer Presidente de la Sociedad, sirviendo entre 1984 y 1987.  El Prof. Dr. Stamboulian ejerció como Profesor de Infectología de la Universidad de Buenos Aires desde 1987 hasta 2006.  En 2000 fue designado Profesor Emérito de Infectología de la  Universidad de Ciencias Empresariales y Sociales (UCES) y bajo recomendación del Departamento de Medicina, la Universidad de Miami designó al Dr. Stamboulian como Profesor Voluntario en Junio de 2008.</p>\r\n<p><br />Profesor Dr. Daniel Stamboulian es Fundador y Presidente de la Fundación de investigaciones y educación FUNCEI (Fundación Centro de Estudios Infectológicos) en Buenos Aires, Argentina. En FUNCEI, el Prof. Dr. Stamboulian ha conducido numerosos programas académicos, de educación e investigación.  En 2002 fundó FIDEC (Fighting Infectious Diseases in Emerging Countries), una organización sin fines de lucro creada en los Miami, Estados Unidos, cuyo objetivo es llevar a cabo programas de prevención de enfermedades infecciosas en países emergentes. A través de FIDEC (Fighting Infectious Diseases in Emerging Countries), el Prof. Dr. Stamboulian ha establecido como objetivo promover un enfoque regional y multidisciplinario para la prevención y manejo de las enfermedades infecciosas. Para lograr este objetivo, ha organizado varios Grupos de Trabajo, tales como el de Vacunación para Adolescentes y Adultos, Medicina del Viajero, Herpes Zoster, entre otros.  A través de estos grupos de trabajo, especialistas desarrollan y llevan a cabo acciones en las áreas de investigación y prevención, diagnóstico y manejo de enfermedades infecciosas. </p>\r\n<div> </div>\r\n<div>FIDEC (Fighting Infectious Diseases in Emerging Countries), Fundador<br />Miami, Estados Unidos<br /><br />FUNCEI (Fundación Centro de Estudios Infectológicos), Presidente<br />Buenos Aires, Argentina<br /><br />UCES (Universidad de Ciencias Empresariales y Sociales), Profesor Emérito de Infectología, Buenos Aires, Argentina<br /><br />Universidad de Miami-Miller School of Medicine, Profesor Voluntario<br />Miami, Estados Unidos</div>', '<p>Introduccion, blabla.</p>', '<p>Epigrafe, blabla.</p>', 'lsiamo1iB-Q', 'underb.jpg,Untitled-2.jpg', '', 'cv.pdf', 1, 2, '2016-02-22 12:32:05', '2016-02-22 12:32:05'),
(3, 30, 6, 'test', 3, 'y', '', '', '', '', '', '', '', 1, 3, '2016-02-25 17:39:53', '2016-02-25 17:39:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido_propositos`
--

CREATE TABLE IF NOT EXISTS `contenido_propositos` (
  `id` smallint(6) unsigned NOT NULL,
  `id_subcategoria` tinyint(3) unsigned NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `contenido` text NOT NULL,
  `formato` tinyint(3) unsigned NOT NULL,
  `activo` tinyint(1) unsigned NOT NULL,
  `orden` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido_que_hacemos`
--

CREATE TABLE IF NOT EXISTS `contenido_que_hacemos` (
  `id` smallint(5) unsigned NOT NULL,
  `id_subcategoria` tinyint(3) unsigned NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `contenido` text NOT NULL,
  `formato` tinyint(3) unsigned NOT NULL,
  `activo` tinyint(1) unsigned NOT NULL,
  `orden` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido_video`
--

CREATE TABLE IF NOT EXISTS `contenido_video` (
  `id` smallint(5) unsigned NOT NULL,
  `id_subcategoria` tinyint(3) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `src` varchar(255) NOT NULL,
  `activo` tinyint(1) unsigned NOT NULL,
  `orden` smallint(5) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestor`
--

CREATE TABLE IF NOT EXISTS `gestor` (
  `id` smallint(5) unsigned NOT NULL,
  `id_modulo` smallint(5) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `contenido` varchar(255) NOT NULL,
  `custom` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `con_categoria` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `con_formato` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `orden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gestor`
--

INSERT INTO `gestor` (`id`, `id_modulo`, `nombre`, `contenido`, `custom`, `con_categoria`, `con_formato`, `orden`, `created_at`, `updated_at`) VALUES
(1, 3, 'Que hacemos', 'nota', 0, 1, 1, 6, '2015-12-11 17:30:45', '2015-12-11 17:30:45'),
(2, 3, 'Propositos', 'nota', 0, 1, 1, 1, '2015-12-21 14:08:38', '2015-12-21 14:08:38'),
(3, 4, 'Videos', 'video', 1, 1, 0, 0, '2015-12-22 13:59:03', '2015-12-22 13:59:03'),
(4, 4, 'Galeria de fotos', 'galeria_de_fotos', 1, 1, 0, 0, '2015-12-22 22:16:25', '2015-12-22 22:16:25'),
(5, 4, 'Audios', 'audio', 1, 1, 0, 0, '2015-12-22 22:16:25', '2015-12-22 22:16:25'),
(6, 5, 'Actualizaciones', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(7, 6, 'Eventos', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(8, 7, 'Publicaciones', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:51', '2016-01-27 21:08:51'),
(9, 3, 'Historia', 'nota', 0, 1, 1, 2, '2015-12-21 14:08:38', '2015-12-21 14:08:38'),
(10, 3, 'Donde actuamos', 'nota', 0, 1, 1, 3, '2015-12-21 14:08:38', '2015-12-21 14:08:38'),
(11, 3, 'Dr. Stamboulian', 'nota', 0, 1, 1, 4, '2015-12-21 14:08:38', '2015-12-21 14:08:38'),
(12, 3, 'Premios', 'nota', 0, 1, 1, 5, '2015-12-21 14:08:38', '2015-12-21 14:08:38'),
(13, 5, 'Educacion', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(14, 5, 'Programas', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(15, 5, 'Publicaciones', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(16, 5, 'Boletin epidemiologico', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(17, 6, 'Educacion', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(18, 6, 'Programas', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(19, 6, 'Campañas', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(20, 6, 'Publicaciones', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(21, 8, 'Novedades', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45'),
(22, 9, 'Sitios relacionados', 'nota', 0, 1, 1, 0, '2016-01-27 21:08:45', '2016-01-27 21:08:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE IF NOT EXISTS `modulo` (
  `id` smallint(5) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `orden` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulo`
--

INSERT INTO `modulo` (`id`, `nombre`, `orden`, `created_at`, `updated_at`) VALUES
(3, 'Quienes somos', 1, '2015-12-11 17:21:39', '2015-12-11 17:21:39'),
(4, 'Multimedia', 6, '2015-12-22 13:57:32', '2015-12-22 13:57:32'),
(5, 'Profesionales', 2, '2016-01-27 21:07:07', '2016-01-27 21:07:07'),
(6, 'Comunidad', 3, '2016-01-27 21:07:37', '2016-01-27 21:07:37'),
(7, 'Investigacion', 4, '2016-01-27 21:07:37', '2016-01-27 21:07:37'),
(8, 'Novedades', 5, '2016-01-27 21:07:37', '2016-01-27 21:07:37'),
(9, 'Sitios relacionados', 7, '2016-01-27 21:07:37', '2016-01-27 21:07:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE IF NOT EXISTS `subcategoria` (
  `id` smallint(5) unsigned NOT NULL,
  `id_gestor` smallint(5) unsigned NOT NULL,
  `slug` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`id`, `id_gestor`, `slug`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 2, 'presentacion', 'presentación', '2015-12-11 17:26:04', '2016-02-22 14:31:01'),
(6, 2, 'propositos', 'propositos', '2015-12-22 13:05:10', '2016-02-22 14:31:08'),
(17, 2, 'planes-de-labor', 'Planes de labor', '2016-02-19 13:42:46', '2016-02-22 14:31:10'),
(18, 2, 'desafios', 'Desafios', '2016-02-19 13:42:59', '2016-02-22 14:31:13'),
(19, 10, 'donde-actuamos', 'Donde actuamos', '2016-02-19 13:44:47', '2016-02-22 14:30:38'),
(20, 9, 'historia', 'Historia', '2016-02-19 13:45:52', '2016-02-22 14:30:55'),
(21, 11, 'dr-stamboulian', 'Dr. Stamboulian', '2016-02-19 13:46:03', '2016-02-22 14:30:51'),
(22, 12, 'premios', 'Premios', '2016-02-19 13:46:15', '2016-02-22 14:30:58'),
(23, 13, 'residencia-de-infectologia', 'Residencia de infectologia', '2016-02-19 13:48:38', '2016-02-22 14:32:00'),
(24, 13, 'formacion-continua', 'Formacion continua', '2016-02-19 13:48:47', '2016-02-22 14:32:02'),
(25, 14, 'control-de-infecciones', 'Control de infecciones', '2016-02-19 13:49:24', '2016-02-22 14:32:04'),
(26, 14, 'farmaceuticos', 'Farmaceuticos', '2016-02-19 13:49:40', '2016-02-22 14:32:07'),
(27, 14, 'medicina-del-viajero', 'Medicina del viajero', '2016-02-19 13:49:52', '2016-02-22 14:32:09'),
(28, 14, 'vacunas-para-adultos', 'Vacunas para adultos', '2016-02-19 13:50:01', '2016-02-22 14:32:12'),
(29, 14, 'programas-concretados', 'Programas concretados', '2016-02-19 13:50:09', '2016-02-22 14:32:14'),
(30, 6, 'ateneos-medicos', 'Ateneos medicos', '2016-02-19 13:50:20', '2016-02-22 14:31:22'),
(31, 6, 'monitoreo-de-publicaciones-cientificas', 'Monitoreo de publicaciones cientificas', '2016-02-19 13:50:29', '2016-02-22 14:31:28'),
(32, 15, 'alimentos', 'Alimentos', '2016-02-19 13:50:39', '2016-02-22 14:32:16'),
(33, 15, 'anuarios', 'Anuarios', '2016-02-19 13:50:50', '2016-02-22 14:32:19'),
(34, 15, 'pediatria', 'Pediatria', '2016-02-19 13:51:00', '2016-02-22 14:32:22'),
(35, 15, 'vacunas-para-adultos', 'Vacunas para adultos', '2016-02-19 13:51:14', '2016-02-22 14:32:24'),
(36, 15, 'viajeros', 'Viajeros', '2016-02-19 13:51:21', '2016-02-22 14:32:26'),
(37, 16, 'boletin-epidemiologico', 'Boletin epidemiologico', '2016-02-19 13:51:30', '2016-02-22 14:31:31'),
(38, 17, 'actividades-escolares', 'Actividades escolares', '2016-02-19 13:53:37', '2016-02-22 14:32:49'),
(39, 17, 'charlas-a-la-comunidad', 'Charlas a la comunidad', '2016-02-19 13:53:45', '2016-02-22 14:32:51'),
(40, 18, 'medicina-del-viajero', 'Medicina del viajero', '2016-02-19 13:54:00', '2016-02-22 14:33:05'),
(41, 18, 'vacunas-para-adultos', 'Vacunas para adultos', '2016-02-19 13:54:16', '2016-02-22 14:33:09'),
(42, 18, 'programas-concretados', 'Programas concretados', '2016-02-19 13:54:24', '2016-02-22 14:33:12'),
(43, 19, 'vigentes', 'Vigentes', '2016-02-19 13:54:41', '2016-02-22 14:32:44'),
(44, 19, 'concretadas', 'Concretadas', '2016-02-19 13:54:47', '2016-02-22 14:32:46'),
(45, 20, 'libros-para-chicos', 'Libros para chicos', '2016-02-19 13:55:00', '2016-02-22 14:33:15'),
(46, 20, 'medicina-del-viajero', 'Medicina del viajero', '2016-02-19 13:55:10', '2016-02-22 14:33:18'),
(47, 20, 'salud-infantil', 'Salud infantil', '2016-02-19 13:55:31', '2016-02-22 14:33:20'),
(48, 20, 'vacunas-para-adultos', 'Vacunas para adultos', '2016-02-19 13:55:40', '2016-02-22 14:33:23'),
(49, 7, 'cena-anual', 'Cena anual', '2016-02-19 13:55:48', '2016-02-22 14:32:53'),
(50, 7, 'eventos-especiales', 'Eventos especiales', '2016-02-19 13:55:58', '2016-02-22 14:32:56'),
(51, 7, 'ferias-de-la-salud', 'Ferias de la salud', '2016-02-19 13:56:05', '2016-02-22 14:32:58'),
(52, 7, 'te-desfile', 'Te-desfile', '2016-02-19 13:56:12', '2016-02-22 14:33:01'),
(53, 7, 'torneos-de-golf', 'Torneos de golf', '2016-02-19 13:56:20', '2016-02-22 14:33:03'),
(54, 8, 'publicaciones', 'Publicaciones', '2016-02-19 13:57:21', '2016-02-22 14:33:28'),
(55, 21, 'novedades', 'Novedades', '2016-02-19 13:58:28', '2016-02-22 14:33:31'),
(56, 5, 'audios', 'Audios', '2016-02-19 14:01:16', '2016-02-22 14:33:35'),
(57, 3, 'a-ciencia-cierta', 'A ciencia cierta', '2016-02-19 14:01:24', '2016-02-22 14:33:44'),
(58, 3, 'las-5-claves', 'Las 5 claves', '2016-02-19 14:01:31', '2016-02-22 14:33:47'),
(59, 3, 'institucional', 'Institucional', '2016-02-19 14:01:40', '2016-02-22 14:33:50'),
(60, 3, 'spots', 'Spots', '2016-02-19 14:01:46', '2016-02-22 14:33:52'),
(61, 4, 'campanas', 'Campañas', '2016-02-19 14:02:01', '2016-02-22 14:33:38'),
(62, 4, 'educacion', 'Educacion', '2016-02-19 14:02:07', '2016-02-22 14:33:40'),
(63, 4, 'eventos', 'Eventos', '2016-02-19 14:02:14', '2016-02-22 14:33:42'),
(64, 22, 'sitios-relacionados', 'Sitios relacionados', '2016-02-19 14:03:31', '2016-02-22 14:34:00'),
(65, 1, 'que-hacemos', 'Que hacemos', '2016-02-19 15:35:09', '2016-02-22 14:31:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test@test.com', '$2y$10$esPyPNaoAjeWQeOu4Y6.6eeQVLmMhVpIptquVRdC3E0E29TPXGoFC', 'SZahESOU55IZI8ZemoNVAuVlJmhaCAeesbVoLuhjLR5e1smINjJwVdasahQ6', '2015-12-11 17:30:41', '2016-01-30 23:40:02');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campo`
--
ALTER TABLE `campo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido_audio`
--
ALTER TABLE `contenido_audio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido_galeria_de_fotos`
--
ALTER TABLE `contenido_galeria_de_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido_nota`
--
ALTER TABLE `contenido_nota`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido_propositos`
--
ALTER TABLE `contenido_propositos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido_que_hacemos`
--
ALTER TABLE `contenido_que_hacemos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido_video`
--
ALTER TABLE `contenido_video`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gestor`
--
ALTER TABLE `gestor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `campo`
--
ALTER TABLE `campo`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contenido_audio`
--
ALTER TABLE `contenido_audio`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contenido_galeria_de_fotos`
--
ALTER TABLE `contenido_galeria_de_fotos`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contenido_nota`
--
ALTER TABLE `contenido_nota`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `contenido_propositos`
--
ALTER TABLE `contenido_propositos`
  MODIFY `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contenido_que_hacemos`
--
ALTER TABLE `contenido_que_hacemos`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contenido_video`
--
ALTER TABLE `contenido_video`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `gestor`
--
ALTER TABLE `gestor`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `modulo`
--
ALTER TABLE `modulo`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
