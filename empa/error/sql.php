
<!DOCTYPE html> 
<html lang="es"> 
	<head>
		<title>EMPA - Escuela de Música Popular de Avellaneda</title>
		<link type="text/css" rel="stylesheet" href="css/estilos.css"/>
		<meta charset=utf-8 />
 		<meta name="description" content="Primera escuela Sudámericana de Musica popular de avellaneda. Formación básica, formación docente, profesorado de instrumento, Areas de jazz, tango y folklore"/>
		<meta name="keywords" content="escuela,musica,popular,carreras,instrumentista,tango,jazz,folklore,instrumento,avellaneda,musico" /> 

	</head>
	<body>
		<div id="trans">
			<section>
				<?php echo '<p>'.$msg.'</p>'; ?>
			</section>
		</div>
	</body>
</html>

