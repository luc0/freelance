<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"></head><body><table class="w640" width="640" cellpadding="0" cellspacing="0" border="0" style="margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;">
                <tbody><tr style="border-collapse:collapse;"><td class="w640" width="640" height="20" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td></tr>
                
                
                <tr style="border-collapse:collapse;">
                <td id="header" class="w640" width="640" align="center" bgcolor="#FFFFFF" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
    
    <div align="center" style="text-align:center;">
        
        <img id="customHeaderImage" label="Header Image" width="640" src="https://i2.createsend1.com/ti/t/F6/7C9/F22/090326/images/empahead.190216.jpg" class="w640" border="0" align="top" style="display:inline;outline-style:none;text-decoration:none;">
        
    </div>
    
    
</td>
                </tr>
                
                <tr style="border-collapse:collapse;"><td class="w640" width="640" height="30" bgcolor="#ffffff" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td></tr>
                <tr id="simple-content-row" style="border-collapse:collapse;"><td class="w640" width="640" bgcolor="#ffffff" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
    <table class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
        <tbody><tr style="border-collapse:collapse;">
            <td class="w30" width="30" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td>
            <td class="w580" width="580" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
                
                        <table class="w580" width="580" cellpadding="0" cellspacing="0" border="0">
                            <tbody><tr style="border-collapse:collapse;">
                                <td class="w580" width="580" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
                                    <p align="left" class="article-title" style="font-size:18px;line-height:24px;color:#b0b0b0;font-weight:bold;margin-top:0px;margin-bottom:18px;font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;">CONSTANCIA DE ALUMNO REGULAR</p>
                                    <div align="left" class="article-content" style="font-size:13px;line-height:18px;color:#444444;margin-top:0px;margin-bottom:18px;font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;">
                                        <p style="margin-bottom:15px;">
    Se deja constancia de que, a la fecha [fecha], [nombre] [apellido], DNI [dni] es alumno/a <strong>regular</strong> de [area] - [nivel] -</p>
<p style="margin-bottom:15px;">
    A pedido del interesado/a y para ser presentada ante quien corresponda, se extiende la presente en la ciudad de Avellaneda a los [dias] d&iacute;as del mes de [mes] de [ano]</p>
                                    </div>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse;"><td class="w580" width="580" height="10" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td></tr>
                        </tbody></table>
                    
            </td>
            <td class="w30" width="30" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td>
        </tr>
    </tbody></table>
</td></tr><tr id="simple-content-row" style="border-collapse:collapse;"><td class="w640" width="640" bgcolor="#ffffff" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
    <table class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
        <tbody><tr style="border-collapse:collapse;">
            <td class="w30" width="30" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td>
            <td class="w640" width="640" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
                
                        <table class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
                            <tbody><tr style="border-collapse:collapse;">
                                <td class="w640" width="640" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
                                    
                                    <div align="left" class="article-content" style="font-size:13px;line-height:18px;color:#444444;margin-top:0px;margin-bottom:18px;font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;">
                                        <table style="margin-top:80px;">
                                            <tbody>
                                                <tr style="border-collapse:collapse;text-align:center;"><td width="30px"></td>
                                                    
                                                    <td width="210px">
                                                        <p style="margin:0;">.......................</p>
                                                        <p style="margin-bottom:15px;">Firma y sello aclaratorio del preceptor</p>
                                                    </td>
                                                    <td width="210px"><p style="margin-bottom:15px;">Sello de la instituci&oacute;n</p></td>
                                                    <td width="210px">
                                                        <p style="margin:0;">.......................</p>
                                                        <p style="margin-bottom:15px;">Firma y sello aclaratorio del Director / Secretario</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table><table>
                                    
                                
                            
                            <tbody><tr style="border-collapse:collapse;"><td class="w580" width="580" height="10" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td></tr>
                        </tbody></table>
                    
            </div></td>
            <td class="w30" width="30" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td>
        </tr>
    </tbody></table>
</td></tr>
                <tr style="border-collapse:collapse;"><td class="w640" width="640" height="15" bgcolor="#ffffff" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td></tr>
                
                
                <tr style="border-collapse:collapse;"><td class="w640" width="640" height="60" style="font-family:'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;"></td></tr>
            </tbody></table></td></tr></tbody></table></body></html>