<?php require_once("conexion.php");session_start();include_once("funciones.php");
	require_once('inyeccion.php');
	if(($_GET["accion"]) == 'salirAdmin'){
		session_destroy();
		session_unset();
		header("Location:index.php");
	}else if(!isset($_SESSION[md5('adminSesion')])){
		$ingresoAdmin = false;
		$usuario = $_POST["usuario"];
		$clave = md5($_POST["clave"]);
		if(!empty($usuario) && !empty($clave) && ctype_alnum($usuario) && strlen($usuario) < 8){//ctype_alnum booleano de si pertenece a [a-zA-Z]
			mysql_select_db($bd_admin,$conexionAdmin);
			$consulta = sprintf("SELECT * FROM admin WHERE ((usuario='%s' AND clave='%s'))", mysql_real_escape_string($usuario), mysql_real_escape_string($clave));
			$resp = mysql_query($consulta,$conexionAdmin);
			//echo $consulta;
			if($datos = mysql_fetch_array($resp)){
			//	echo "sesion";
				//if($datos['usuario'] == $usuario && $datos['clave'] == $clave){
					$_SESSION[md5('adminSesion')] = $usuario;
					$ingresoAdmin = true;

				//}
			}else{
				//echo $usuario." ".$clave;
				$ingresoAdmin = false;
			}
		}
	}
	$registrosMostrados = 10;

?>
<!DOCTYPE html> 

<html lang="es"> 
	<head>
		<title>EMPA - Escuela de Música Popular de Avellaneda</title>
		<link type="text/css" rel="stylesheet" href="css/estilos.css"/>
		<meta charset=utf-8 />
 		<meta name="description" content="Primera escuela Sudámericana de Musica popular de avellaneda. Formación básica, formación docente, profesorado de instrumento, Areas de jazz, tango y folklore"/>
		<meta name="keywords" content="escuela,musica,popular,carreras,instrumentista,tango,jazz,folklore,instrumento,avellaneda,musico" /> 

		<!--<link rel="stylesheet" type="text/css" href="js/progressbar/uicore.css"> 
		<link rel="stylesheet" type="text/css" href="js/progressbar/uitheme.css">
		<link rel="stylesheet" type="text/css" href="js/progressbar/progressbar.css">-->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/dinamico.js"></script>
		<!--<script type="text/javascript" src="js/progressbar/uicore.js"></script>
		<script type="text/javascript" src="js/progressbar/uiwidget.js"></script>
		<script type="text/javascript" src="js/progressbar/progressbar.js"></script>-->
		<!--<script type="text/javascript" src="js/progressbar/ui.js"></script>-->
		<style>
			#tabla{height:400px;}
		</style>

<!--	<link type="text/css" href="jquery-ui-1.7.2.custom/css/custom-theme/jquery-ui-1.7.2.custom.css" rel="stylesheet" /> 
		<script type="text/javascript" src="jquery-ui-1.7.2.custom/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="jquery-ui-1.7.2.custom/js/jquery-ui-1.7.2.custom.min.js"></script>-->

		<!--<link rel="shortcut icon" href="favicon.ico" />--> 

		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]--> 
		<!--[if IE 7]>
		<style> nav{padding:0px;margin-left:-165px;}</style>
		<![endif]-->



	</head>


	<body>
		<?php

			//echo detectMaxUploadFileSize();
			$max_upload = (int)(ini_get('upload_max_filesize'));
			$max_post = (int)(ini_get('post_max_size'));
			$memory_limit = (int)(ini_get('memory_limit'));
			$upload_mb = min($max_upload, $max_post, $memory_limit);
			/*echo "$max_upload<br/>";
			echo "$max_post<br/>";
			echo $memory_limit;*/
		?>
<!--<div id="contenedor"></div><label id="porcentaje">0%</label>-->
 <script>
	//crea barra progreso
	$(function(){
		$("#contenedor").progressbar();
	})
	//muestra el porcentaje
	$("#contenedor").progressbar({ change: function() {   
	    $("#porcentaje").text($("#contenedor").progressbar("option", "value") + "%");  
	}});
	//lleno = se borra
	$("#contenedor").progressbar({ 
		change: function() {   
	    		$("#porcentaje").text($("#contenedor").progressbar("option", "value") + "%");  
		},
		complete: function() {   
	    		$('#porcentaje').remove()  
			$('#contenedor').remove()
		}

	});
	
	//$( "#contenedor" ).progressbar({value:10});

  </script>


	</div>

		<div id="transAdm">
		<div id="cabeceraAdm">
				<header>
					<div>
						<p>Provincia de Buenos Aires</p>
						<p>Direccion General de Cultura y Educación</p>
						<p>Subsecretaria de Educación</p>
						<p>Dirección de Educación Artistica</p>
					</div>

					<h1><a href="/" title="Escuela de Música Popular"><span>Escuela de Música Popular de Avellaneda</span></a></h1>
				</header>
		</div>	
		<div id="contenedor" >		
			<div>

				<?php
					if(!empty($_SESSION[md5('adminSesion')])){
					
						$accion = $_GET["accion"];
						switch($accion){


							//BORRAR MATRICULADOS //

							case 'borrarMatriculados':
								$borrarIngresantes = "UPDATE matriculados SET visible = 0";
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query($borrarIngresantes,$conexion);
								if($resp){
									echo "<div id='validacionOk'>Se borraron los Matriculados</div>";
								}else{
									echo '<div id="validacionAlert">No se pudo borrar :( intente mas tarde</div>';
								}
								break;

							//BORRAR INGRESANTES //

							case 'borrarIngresantes':
								$borrarIngresantes = "UPDATE ingresantes SET visible = 0";
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query($borrarIngresantes,$conexion);
								if($resp){
									echo "<div id='validacionOk'>Se borraron los Ingresantes</div>";
								}else{
									echo '<div id="validacionAlert">No se pudo borrar :( intente mas tarde.</div>';
								}
								break;

							//BORRAR INSCRIP EXAMENES //

							case 'borrarInscrpExamenes':
								$borrarInscrpExamenes = "UPDATE inscripcionExamenes SET visible = 0";
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query($borrarInscrpExamenes,$conexion);
								if($resp){
									echo "<div id='validacionOk'>Se borraron los Inscriptos a exámenes</div>";
								}else{
									echo '<div id="validacionAlert">No se pudo borrar :( intente mas tarde.</div>';
								}
								break;

		// INGRESANTES //

							case 'verIngresantes':
							?>
							<div id="busqueda">
								<form method="get" action="?">
									<input type="hidden" name="accion" value="verIngresantes">
									<input type="hidden" name="p" value="1">
									<label>Buscar por:</label>
									<select name="tipoBuscar">
										<option value="id">Id</option>
										<option value="nombreApellido">Nombre y Apellido</option>
										<option value="dni">Número de Documento</option>
										<option value="instrumento">Instrumento</option>
										<option value="localidad">Localidad</option>
									</select>
									<input type="text" name="valorBuscar" />
									<label>Ordenar por:</label>
									<select name="ordenar">
										<option value="id">Id</option>
										<option value="nombre">Nombre</option>
										<option value="ap'ellido">Apellido</option>
										<option value="sexo">Sexo</option>
										<option value="documento">Número de Documento</option>
										<option value="nacimiento">Nacimiento</option>
										<option value="nacionalidad">Nacionalidad</option>
										<option value="estudios">Estudios</option>
										<option value="instrumento">Instrumento</option>
										<option value="nivel">Nivel</option>
										<option value="email">Email</option>
										<option value="domicilio">Domicilio</option>
										<option value="localidad">Localidad</option>
										<option value="telefono">Telefono</option>
										<option value="fecha">Fecha de Envio</option>
									</select>
									<input type="submit" value="Aceptar" />
								</form>
							</div>

							<?php

								$pag = $_GET['p'];
								$registroInicio = $pag-1;
								$registroInicio *= $registrosMostrados; 
								//busco algo (relleno el input)
								if(!empty($_GET['valorBuscar'])){
									//busco por Nombre y Apellido
									if($_GET['tipoBuscar'] == 'nombreApellido'){
										$filtro = "AND CONCAT(ingresantes.nombre,' ',ingresantes.apellido) LIKE '%".$_GET['valorBuscar']."%'";
									//busco por dni (valor sin comillas)
									}else if($_GET['tipoBuscar'] == 'dni'){
										$filtro = "AND ingresantes.".$_GET['tipoBuscar']." LIKE '%".$_GET['valorBuscar']."%'";
									//busco por cualquier otra
									}else if(!empty($_GET['tipoBuscar'])){
										$filtro = "AND ingresantes.".$_GET['tipoBuscar']." = '".$_GET['valorBuscar']."'";
									}
								}
								$ordenar = $_GET['ordenar'].",";
								if(empty($_GET['buscarId'])){
									$filtroId = '';
								}
								if(empty($_GET['ordenar'])){
									$ordenar = '';
								}
$queryIngresantes = "
SELECT ingresantes.nombre,
ingresantes.apellido,
sexo.valor,
ingresantes.dni,
DATE_FORMAT(ingresantes.nacimiento, '%d/%m/%y') AS fecha,
ingresantes.nacionalidad,
estudios.valor,
instrumentos.valor,
nivel.valor,
ingresantes.email,
ingresantes.domicilio,
ingresantes.localidad,
ingresantes.telefono,
DATE_FORMAT(ingresantes.fecha, '%d/%m/%y %h:%i:%s') AS fecha,
ingresantes.id, 
ingresantes.visible 
FROM ingresantes LEFT JOIN sexo ON ingresantes.id_sexo = sexo.id LEFT JOIN estudios ON ingresantes.id_estudios = estudios.id LEFT JOIN instrumentos ON ingresantes.id_instrumento = instrumentos.id LEFT JOIN nivel ON ingresantes.id_nivel = nivel.id WHERE ingresantes.visible = 1 $filtro ORDER BY $ordenar id DESC"; // al final estaba esto: LIMIT $registroInicio,$registrosMostrados
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query($queryIngresantes,$conexion);
								if($resp){
								//si se ejecuta la query bien con resultado
									echo '<h2>Pagina '.$pag.':  '.mysql_num_rows($resp).' ingresantes</h2>';
									echo '<div id="tabla">
									<table border id="Exportar_a_Excel">
									<tr class="negrita">
										<td>Nombre</td>
										<td>Apellido</td>
										<td>Sexo</td>
										<td>Documento</td>
										<td>Nacimiento</td>
										<td>Nacionalidad</td>
										<td>Estudios</td>
										<td>Instrumento</td>
										<td>Nivel</td>
										<td>Email</td>
										<td>Domicilio</td>
										<td>Localidad</td>
										<td>Teléfono</td>
										<td>Fecha de envio</td>
										<td>Id</td>
									</tr>
									';

									//$excel = '';
									//$registrosCantidad = mysql_num_rows($resp);
									//$contador;
									while($ingresantes = mysql_fetch_array($resp)){
									//	$contador ++;
									//	$porcentaje = round($contador*100/$registrosCantidad);
									//	echo '<script>$( "#contenedor" ).progressbar({value:'.$porcentaje.'});</script>';
										$id = $ingresantes[14];
										if($id % 2){
											$estilo = 'oscuroLista';
										}else{
											$estilo = '';
										}
										//lo que se exporta en excel//
										$excel.="$ingresantes[0]\t$ingresantes[1]\t$ingresantes[2]\t$ingresantes[3]\t$ingresantes[4]\t$ingresantes[5]\t$ingresantes[6]\t$ingresantes[7]\t$ingresantes[8]\t$ingresantes[9]\t$ingresantes[10]\t$ingresantes[11]\t$ingresantes[12]\t$ingresantes[13]\t$ingresantes[14]\n";

										echo '<tr class="'.$estilo.'"><td>'.$ingresantes[0]."</td> <td>".$ingresantes[1]."</td> <td>".$ingresantes[2]."</td> <td>".$ingresantes[3]."</td> <td>".$ingresantes[4]."</td> <td>".$ingresantes[5]."</td> <td>".$ingresantes[6]."</td> <td>".$ingresantes[7]."</td> <td>".$ingresantes[8]."</td> <td>".$ingresantes[9]."</td> <td>".$ingresantes[10]."</td> <td>".$ingresantes[11]."</td> <td>".$ingresantes[12]."</td> <td>".$ingresantes[13]."</td> <td>".$ingresantes[14]."</td> </tr>";
									}
									/*elimina "caracteres escapados" para que no joda cuando descargues excel
									  sirve para cuando alguno pone como calle O'Brien por ejemplo..*/
									$excel = str_replace('\\','',$excel);
									$excel = str_replace('\'','&rsquo;',$excel);
									/**/
									echo '</table></div>';
									$pagAnterior = $pag-1;
									$pagSiguiente = $pag+1;

									echo'<div id="paginas">';
									if($pag > 1){	
										echo '<a href="/adm.php?accion=verIngresantes&p='.$pagAnterior.'" title="ant">Anterior</a>';
									}
									echo'<a href="/adm.php?accion=verIngresantes&p='.$pagSiguiente.'" title="seg">Siguiente</a>';
									echo'
									<div class="clear"></div>
										</div>
									';
								}
								echo "<h3>Descargar Base de datos (excel):</h3>";
								echo "<form action='excel.php' method='POST'><input type='hidden' name='export' value='".$excel."'/><input type='hidden' name='nombreArchivo' value='ingresantes'><input type='submit' value='Decargar Excel de la base de datos'></form>";  
								break;

		// MATRICULADOS //

							case 'verMatriculados':
							?>
							<div id="busqueda">
								<form method="get" action="?">
									<input type="hidden" name="accion" value="verMatriculados">
									<input type="hidden" name="p" value="1">
									<label>Buscar por:</label>
									<select name="tipoBuscar">
										<option value="id">Id</option>
										<option value="nombreApellido">Nombre y Apellido</option>
										<option value="dni">Número de Documento</option>
										<option value="instrumento">Instrumento</option>
										<option value="carrera">Carrera</option>
										<option value="localidad">Localidad</option>
									</select>
									<input type="text" name="valorBuscar" />
									<label>Ordenar por:</label>
									<select name="ordenar">
										<option value="id">Id</option>
										<option value="nombre">Nombre</option>
										<option value="ap'ellido">Apellido</option>
										<option value="documento">Número de Documento</option>
										<option value="nacimiento">Nacimiento</option>
										<!--<option value="nacionalidad">Nacionalidad</option>
										<option value="estudios">Estudios</option>
										<option value="instrumento">Instrumento</option>
										<option value="nivel">Nivel</option>
										<option value="email">Email</option>
										<option value="domicilio">Domicilio</option>
										<option value="localidad">Localidad</option>
										<option value="telefono">Telefono</option>
										<option value="fecha">Fecha de Envio</option>-->
									</select>
									<input type="submit" value="Aceptar" />
								</form>
							</div>

							<?php

								$pag = $_GET['p'];
								$registroInicio = $pag-1;
								$registroInicio *= $registrosMostrados; 
								//busco algo (relleno el input)
								if(!empty($_GET['valorBuscar'])){
									//busco por Nombre y Apellido
									if($_GET['tipoBuscar'] == 'nombreApellido'){
										$filtro = "AND CONCAT(matriculados.nombre,' ',matriculados.apellido) LIKE '%".$_GET['valorBuscar']."%'";
									//busco por dni (valor sin comillas)
									}else if($_GET['tipoBuscar'] == 'dni'){
										$filtro = "AND matriculados.".$_GET['tipoBuscar']." LIKE '%".$_GET['valorBuscar']."%'";
									//busco por cualquier otra
									}else if(!empty($_GET['tipoBuscar'])){
										$filtro = "AND matriculados.".$_GET['tipoBuscar']." = '".$_GET['valorBuscar']."'";
									}
								}
								$ordenar = $_GET['ordenar'].",";
								if(empty($_GET['buscarId'])){
									$filtroId = '';
								}
								if(empty($_GET['ordenar'])){
									$ordenar = '';
								}
								$queryMatriculados = "
SELECT 
matriculados.id,
matriculados.nombre,
matriculados.apellido,
matriculados.sexo,
matriculados.dni_tipo,
matriculados.dni,
DATE_FORMAT(matriculados.nacimiento, '%Y-%m-%d') AS fecha,
matriculados.nacionalidad,
instrumentos.valor AS instrumento,
matriculados.carrera,
matriculados.email,
matriculados.domicilio,
matriculados.localidad,
matriculados.telefono,
matriculados.celular,
matriculados.ano_ingreso,
matriculados.ultimo_aprobado,
matriculados.materia1,
matriculados.materia2,
matriculados.materia3,
matriculados.materia4,
matriculados.materia5,
matriculados.materia6,
matriculados.materia7,
matriculados.materia8,
matriculados.materia9,
matriculados.materia10,
matriculados.materia11,
matriculados.materia12,
matriculados.materia13,
matriculados.materia14,
matriculados.materia15,
matriculados.visible
FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1 $filtro ORDER BY $ordenar id DESC"; //antes estaba: LIMIT $registroInicio,$registrosMostrados
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query($queryMatriculados,$conexion);
								if($resp){
								//si se ejecuta la query bien con resultado
									echo '<h2>Pagina '.$pag.':  '.mysql_num_rows($resp).' matriculados</h2>';
									echo '<div id="tabla">
									<table border>
									<tr class="negrita">
										<td>Id</td>
										<td>Nombre</td>
										<td>Apellido</td>
										<td>Sexo</td>
										<td>Dni Tipo</td>
										<td>Documento</td>
										<td>Nacimiento</td>
										<td>Nacionalidad</td>
										<td>Instrumento</td>
										<td>Carrera</td>
										<td>Email</td>
										<td>Domicilio</td>
										<td>Localidad</td>
										<td>Teléfono</td>
										<td>celular</td>
										<td>año ingreso</td>
										<td>ultimo aprobado</td>
										<td>materia1</td><td>materia2</td><td>materia3</td><td>materia4</td><td>materia5</td><td>materia6</td><td>materia7</td><td>materia8</td><td>materia9</td><td>materia10</td><td>materia11</td><td>materia12</td><td>materia13</td><td>materia14</td><td>materia15</td>
									</tr>
									';

								    // Convierte UTF-8 (DB) a Windows-1252 (encoding del excel)
								    //echo $resp;
									
									//file_put_contents($resp, $data);
									/*function myEncodeFunction(&$item){
									    $item = mb_convert_encoding($item, 'Windows-1252', 'ISO-8859-1');
									}

									array_walk_recursive($resp, 'myEncodeFunction');*/
									/*--------------*/

									while($matriculados = mysql_fetch_array($resp)){
										$id = $matriculados[14];
										if($id % 2){
											$estilo = 'oscuroLista';
										}else{
											$estilo = '';
										}
										//lo que se exporta en excel//
										$excel.="$matriculados[0]\t$matriculados[1]\t$matriculados[2]\t$matriculados[3]\t$matriculados[4]\t$matriculados[5]\t$matriculados[6]\t$matriculados[7]\t$matriculados[8]\t$matriculados[9]\t$matriculados[10]\t$matriculados[11]\t$matriculados[12]\t$matriculados[13]\t$matriculados[14]\t$matriculados[15]\t$matriculados[16]\t$matriculados[17]\t$matriculados[18]\t$matriculados[19]\t$matriculados[20]\t$matriculados[21]\t$matriculados[22]\t$matriculados[23]\t$matriculados[24]\t$matriculados[25]\t$matriculados[26]\t$matriculados[27]\t$matriculados[28]\t$matriculados[29]\t$matriculados[30]\t$matriculados[31]\n";

										echo '<tr class="'.$estilo.'"><td>'.$matriculados[0]."</td> <td>".$matriculados[1]."</td> <td>".$matriculados[2]."</td> <td>".$matriculados[3]."</td> <td>".$matriculados[4]."</td> <td>".$matriculados[5]."</td> <td>".$matriculados[6]."</td> <td>".$matriculados[7]."</td> <td>".$matriculados[8]."</td> <td>".$matriculados[9]."</td> <td>".$matriculados[10]."</td> <td>".$matriculados[11]."</td> <td>".$matriculados[12]."</td> <td>".$matriculados[13]."</td> <td>".$matriculados[14]."</td> <td>".$matriculados[15]."</td> <td>".$matriculados[16]."</td> <td>".$matriculados[17]."</td> <td>".$matriculados[18]."</td> <td>".$matriculados[19]."</td> <td>".$matriculados[20]."</td> <td>".$matriculados[21]."</td> <td>".$matriculados[22]."</td> <td>".$matriculados[23]."</td> <td>".$matriculados[24]."</td> <td>".$matriculados[25]."</td> <td>".$matriculados[26]."</td> <td>".$matriculados[27]."</td> <td>".$matriculados[28]."</td> <td>".$matriculados[29]."</td> <td>".$matriculados[30]."</td> <td>".$matriculados[31]."</td> </tr>";
									}
									echo '</table></div>';
									$pagAnterior = $pag-1;
									$pagSiguiente = $pag+1;

									echo'<div id="paginas">';
									if($pag > 1){	
										echo '<a href="/adm.php?accion=verMatriculados&p='.$pagAnterior.'" title="ant">Anterior</a>';
									}
									echo'<a href="/adm.php?accion=verMatriculados&p='.$pagSiguiente.'" title="seg">Siguiente</a>';
									echo'
									<div class="clear"></div>
										</div>
									';
								}
//ORDENAR POR MATERIAS (matriculados)
$queryMatriculados = "SELECT * FROM ((
SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia1,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia2,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia3,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia4,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia5,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia6,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia7,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia8,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia9,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia10,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia11,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia12,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia13,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia14,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)UNION(SELECT matriculados.id,matriculados.nombre,matriculados.apellido,matriculados.sexo,matriculados.dni_tipo,matriculados.dni,DATE_FORMAT(matriculados.nacimiento, '%d/%m/%y') AS fecha,matriculados.nacionalidad,instrumentos.valor AS instrumento,matriculados.carrera,matriculados.email,matriculados.domicilio,matriculados.localidad,matriculados.telefono,matriculados.celular,matriculados.ano_ingreso,matriculados.ultimo_aprobado,matriculados.materia15,matriculados.visible FROM matriculados LEFT JOIN instrumentos ON matriculados.id_instrumento = instrumentos.id WHERE matriculados.visible = 1
)) t ORDER BY dni DESC";
								mysql_select_db($bd_ingresantes,$conexion);
								$respOrdenPorMaterias = mysql_query($queryMatriculados,$conexion);	

								if($respOrdenPorMaterias){
									while($anotados = mysql_fetch_array($respOrdenPorMaterias)){
										/*lo que exporta al excel (ORDENADO POR MATERIAS, con alumnos repetidos)*/
										$excelPorMateria.="$anotados[0]\t$anotados[1]\t$anotados[2]\t$anotados[3]\t$anotados[4]\t$anotados[5]\t$anotados[6]\t$anotados[7]\t$anotados[8]\t$anotados[9]\t$anotados[10]\t$anotados[11]\t$anotados[12]\t$anotados[13]\t$anotados[14]\t$anotados[15]\t\n";
									}
								}

								//$excel = mb_convert_encoding($excel, 'Windows-1252' , 'UTF-8' );
								$excel = mb_convert_encoding($excel, 'Windows-1252' , 'latin1' );

								echo "<h3>Descargar Base de datos (excel):</h3>";
								echo "<form action='excel.php' method='POST'><textarea name='export' style='display:none'>".$excelPorMateria."</textarea><input type='hidden' name='nombreArchivo' value='matriculacion'><input type='submit' value='Decargar (por materia)'></form>";
								echo "<form action='excel.php' method='POST'><textarea name='export' style='display:none'>".$excel."</textarea><input type='hidden' name='nombreArchivo' value='matriculacion'><input type='submit' value='Decargar'></form>";
								break;

							//----------------------------//


		// EXAMENES //

							case 'verExamenes':
							?>
							<!--<div id="busqueda">
								<form method="get" action="?">
									<input type="hidden" name="accion" value="verExamenes">
									<input type="hidden" name="p" value="1">
									<label>Buscar por:</label>
									<select name="tipoBuscar">
										<option value="id">Id</option>
										<option value="nombreApellido">Nombre y Apellido</option>
										<option value="dni">Número de Documento</option>
										<option value="instrumento">Instrumento</option>
										<option value="localidad">Localidad</option>
									</select>
									<input type="text" name="valorBuscar" />
									<label>Ordenar por:</label>
									<select name="ordenar">
										<option value="id">Id</option>
										<option value="nombre">Nombre</option>
										<option value="ap'ellido">Apellido</option>
										<option value="documento">Número de Documento</option>
										<option value="nacimiento">Nacimiento</option>

									</select>
									<input type="submit" value="Aceptar" />
								</form>
							</div>-->

							<?php

								$pag = $_GET['p'];
								$registroInicio = $pag-1;
								$registroInicio *= $registrosMostrados; 
								//busco algo (relleno el input)
								if(!empty($_GET['valorBuscar'])){
									//busco por Nombre y Apellido
									if($_GET['tipoBuscar'] == 'nombreApellido'){
										$filtro = "AND CONCAT(matriculados.nombre,' ',matriculados.apellido) LIKE '%".$_GET['valorBuscar']."%'";
									//busco por dni (valor sin comillas)
									}else if($_GET['tipoBuscar'] == 'dni'){
										$filtro = "AND matriculados.".$_GET['tipoBuscar']." LIKE '%".$_GET['valorBuscar']."%'";
									//busco por cualquier otra
									}else if(!empty($_GET['tipoBuscar'])){
										$filtro = "AND matriculados.".$_GET['tipoBuscar']." = '".$_GET['valorBuscar']."'";
									}
								}
								$ordenar = $_GET['ordenar'].",";
								if(empty($_GET['buscarId'])){
									$filtroId = '';
								}
								if(empty($_GET['ordenar'])){
									$ordenar = '';
								}
								$queryExamenes = "
SELECT 
inscripcionExamenes.id,
inscripcionExamenes.nombre,
inscripcionExamenes.apellido,
inscripcionExamenes.dni,
inscripcionExamenes.email,
instrumentos.valor AS instrumento,
inscripcionExamenes.telefono,
inscripcionExamenes.materia1,
inscripcionExamenes.materia2,
inscripcionExamenes.materia3,
inscripcionExamenes.materia4,
inscripcionExamenes.materia5,
inscripcionExamenes.materia6,
inscripcionExamenes.materia7,
inscripcionExamenes.materia8,
inscripcionExamenes.materia9,
inscripcionExamenes.materia10,
inscripcionExamenes.materia11,
inscripcionExamenes.materia12,
inscripcionExamenes.materia13,
inscripcionExamenes.materia14,
inscripcionExamenes.materia15,
inscripcionExamenes.visible
FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1 $filtro ORDER BY $ordenar id DESC"; //antes estaba: LIMIT $registroInicio,$registrosMostrados
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query($queryExamenes,$conexion);
								if($resp){
								//si se ejecuta la query bien con resultado
									echo '<h2>Pagina '.$pag.':  '.mysql_num_rows($resp).' Inscriptos a examenes</h2>';
									echo '<div id="tabla">
									<table border>
									<tr class="negrita">
										<td>Id</td>
										<td>Nombre</td>
										<td>Apellido</td>
										<td>Documento</td>
										<td>Instrumento</td>
										<td>Email</td>
										<td>Teléfono</td>
										<td>materia1</td><td>materia2</td><td>materia3</td><td>materia4</td><td>materia5</td><td>materia6</td><td>materia7</td><td>materia8</td><td>materia9</td><td>materia10</td><td>materia11</td><td>materia12</td><td>materia13</td><td>materia14</td><td>materia15</td>
									</tr>
									';
									while($inscrExamenes = mysql_fetch_array($resp)){
										$id = $inscrExamenes[14];
										if($id % 2){
											$estilo = 'oscuroLista';
										}else{
											$estilo = '';
										}
										//lo que se exporta en excel//
										/*DEJARLO por si quiero que lo exporte como en la bd*/
										$excel.="$inscrExamenes[0]\t$inscrExamenes[1]\t$inscrExamenes[2]\t$inscrExamenes[3]\t$inscrExamenes[4]\t$inscrExamenes[5]\t$inscrExamenes[6]\t$inscrExamenes[7]\t$inscrExamenes[8]\t$inscrExamenes[9]\t$inscrExamenes[10]\t$inscrExamenes[11]\t$inscrExamenes[12]$inscrExamenes[13]\t$inscrExamenes[14]\t$inscrExamenes[15]\t$inscrExamenes[16]\t$inscrExamenes[17]\t$inscrExamenes[18]\t$inscrExamenes[19]\t$inscrExamenes[20]\t$inscrExamenes[21]\t\n";

										echo '<tr class="'.$estilo.'"><td>'.$inscrExamenes[0]."</td> <td>".$inscrExamenes[1]."</td> <td>".$inscrExamenes[2]."</td> <td>".$inscrExamenes[3]."</td> <td>".$inscrExamenes[4]."</td> <td>".$inscrExamenes[5]."</td> <td>".$inscrExamenes[6]."</td> <td>".$inscrExamenes[7]."</td> <td>".$inscrExamenes[8]."</td> <td>".$inscrExamenes[9]."</td> <td>".$inscrExamenes[10]."</td> <td>".$inscrExamenes[11]."</td> <td>".$inscrExamenes[12]."</td> <td>".$inscrExamenes[13]."</td> <td>".$inscrExamenes[14]."</td> <td>".$inscrExamenes[15]."</td> <td>".$inscrExamenes[16]."</td> <td>".$inscrExamenes[17]."</td> <td>".$inscrExamenes[18]."</td> <td>".$inscrExamenes[19]."</td> <td>".$inscrExamenes[20]."</td> <td>".$inscrExamenes[21]."</td> </tr>";
									}
									echo '</table></div>';
									$pagAnterior = $pag-1;
									$pagSiguiente = $pag+1;

									echo'<div id="paginas">';
									if($pag > 1){	
										echo '<a href="/adm.php?accion=verExamenes&p='.$pagAnterior.'" title="ant">Anterior</a>';
									}
									echo'<a href="/adm.php?accion=verExamenes&p='.$pagSiguiente.'" title="seg">Siguiente</a>';
									echo'
									<div class="clear"></div>
										</div>
									';
								}

//ORDENAR POR MATERIAS (examenes)
$queryExamenes = "SELECT * FROM ((
SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia1,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia2,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia3,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia4,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia5,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia6,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia7,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia8,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia9,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia10,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia11,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia12,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia13,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia14,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)UNION(SELECT inscripcionExamenes.id,inscripcionExamenes.nombre,inscripcionExamenes.apellido,inscripcionExamenes.dni,inscripcionExamenes.email,instrumentos.valor AS instrumento,inscripcionExamenes.telefono,inscripcionExamenes.materia15,inscripcionExamenes.visible FROM inscripcionExamenes LEFT JOIN instrumentos ON inscripcionExamenes.id_instrumento = instrumentos.id WHERE inscripcionExamenes.visible = 1
)) t ORDER BY id DESC ";
								mysql_select_db($bd_ingresantes,$conexion);
								$respOrdenPorMaterias = mysql_query($queryExamenes,$conexion);	

									if($respOrdenPorMaterias){
										while($inscrExamenes = mysql_fetch_array($respOrdenPorMaterias)){
											/*lo que exporta al excel (ORDENADO POR MATERIAS, con alumnos repetidos)*/
											$excelPorMateria.="$inscrExamenes[0]\t$inscrExamenes[1]\t$inscrExamenes[2]\t$inscrExamenes[3]\t$inscrExamenes[4]\t$inscrExamenes[5]\t$inscrExamenes[6]\t$inscrExamenes[7]\t\n";
										}
									}
									echo "<h3>Descargar Base de datos (excel):</h3>";
									echo "<form action='excel.php' method='POST'><textarea name='export' style='display:none'>".$excelPorMateria."</textarea><input type='hidden' name='nombreArchivo' value='examenes'><input type='submit' value='Decargar (por materia)'></form>";
									echo "<form action='excel.php' method='POST'><textarea name='export' style='display:none'>".$excel."</textarea><input type='hidden' name='nombreArchivo' value='examenes'><input type='submit' value='Decargar'></form>";
									break;

							//----------------------------//

							case 'instrumentos':
								mysql_select_db($bd_ingresantes,$conexion);
								$resp = mysql_query("SELECT * from instrumentos WHERE activo = 1",$conexion);
								echo '<div class="padding"><div class="opcionesAdmin"><ul><li><a href="/adm.php?accion=agregarInstrumento">Agregar Instrumento</a></li>';
								echo '<li class="boton">Borrar: <select id="borrarInstrumento" name="instrumento"><option value="">Seleccione algun instrumento para borrar</option>';
								while($datos = mysql_fetch_array($resp)){
									echo '<option value="'.$datos['id'].'">'.$datos['valor'].'</option>';
										echo "<td>".$datos['valor']."</td>".'<td><a href="/adm.php?accion=instrumentos&borrar='.$datos['id'].'">Borrar</a></td>';
								}
								echo '</select><div class="oculto"><a href="/adm.php?">¡Borrar!</a></div></li></ul></div></div>';
								break;
							case 'agregarInstrumento';
								echo '<div class="padding">
								<form method="get" action="?">
									<input type="hidden" name="accion" value="instrumentoNuevo">
									<label>Nombre del instrumento: </label><input type="text" name="instrumentoNuevo" />
									<input type="submit" value="Agregar" />
								</form>
								</div>
								';
								break;
							case 'instrumentoNuevo';
								mysql_select_db($bd_ingresantes,$conexion);
								$instrumentoNuevo = $_GET["instrumentoNuevo"];
								if($resp = mysql_query("INSERT into instrumentos(valor,activo) VALUES('$instrumentoNuevo',1)",$conexion)){
									echo '<div id="validacionOk">Se creó un nuevo instrumento</div>';
								}else{
									echo '<div id="validacionAlert">No se pudo crear el instrumento, intente de nuevo.</div>';
								}
								break;
							case 'borrarInstrumento';
								mysql_select_db($bd_ingresantes,$conexion);
								$idBorrar = $_GET["id"];
								//if($resp = mysql_query("DELETE FROM instrumentos WHERE id='$idBorrar'",$conexion)){
								if($resp = mysql_query("UPDATE instrumentos SET activo = 0 WHERE id='$idBorrar'",$conexion)){
									echo '<div id="validacionOk">Se borró el instrumento</div>';
								}else{
									echo '<div id="validacionAlert">No se pudo borrar el instrumento, intente de nuevo.</div>';
								}
								break;

						}
						/*if($_POST){
							echo '<div id="validacionOk">';
								echo 'Ok';
							echo '</div>';
						}*/
						echo "<h2>Opciones de Administrador</h2>";
						echo "<div class='opcionesAdmin'><ul>";
						echo "<h3>Pagina:</h3>";
						echo "<li><a href='/index.php'>Editar Contenido</a></li>";
						echo "<h3>Ver Base de datos Online:</h3>";
						echo "<li><a href='/adm.php?accion=verIngresantes&p=1'>Ver todos los ingresantes</a></li>";
						echo "<li><a href='/adm.php?accion=verMatriculados&p=1'>Ver todos los matriculados</a></li>";
						echo "<li><a href='/adm.php?accion=verExamenes&p=1'>Ver todos los inscriptos a exámenes</a></li>";
					//	echo "<h3>Descargar Base de datos (excel):</h3>";
					//	echo "<li><a href='/adm.php?accion=descargarIngresantes'>Descargar planilla de ingresantes</a></li>";
					//	echo "<li><a href='/adm.php?accion=descargarMatriculados'>Descargar planilla de Matriculados</a></li>";
						echo "<h3>Borrar de la base de datos:</h3>";
						echo "<li><a href='/adm.php?accion=borrarIngresantes'>Borrar todos los Ingresantes</a></li>";
						echo "<li><a href='/adm.php?accion=borrarMatriculados'>Borrar todos los Matriculados</a></li>";
						echo "<li><a href='/adm.php?accion=borrarInscrpExamenes'>Borrar todos los Inscriptos a exámenes</a></li>";
						//echo "<li><a href='/adm.php?accion=instrumentos'>Agregar/Eliminar Instrumentos</a></li>";
						echo "<h3>Estado de planillas online:</h3>";
						mysql_select_db($bd_ingresantes,$conexion);
						if(!empty($_GET['opciones'])){
							//aplico nuevas opciones para ingreso
							if($_GET['inscripcion']){
								$resp = mysql_query("UPDATE opciones SET activado = 1 WHERE id = 1",$conexion);
							}else{
								$resp = mysql_query("UPDATE opciones SET activado = 0 WHERE id = 1",$conexion);
							}
							//aplico nuevas opciones para matriculacion
							if($_GET['matriculacion']){
								$resp = mysql_query("UPDATE opciones SET activado = 1 WHERE id = 2",$conexion);
							}else{
								$resp = mysql_query("UPDATE opciones SET activado = 0 WHERE id = 2",$conexion);
							}
							//aplico nuevas opciones para incripcion examenes
							if($_GET['examenes']){
								$resp = mysql_query("UPDATE opciones SET activado = 1 WHERE id = 3",$conexion);
							}else{
								$resp = mysql_query("UPDATE opciones SET activado = 0 WHERE id = 3",$conexion);
							}
							if($_GET['alumnoRegular']){
								$resp = mysql_query("UPDATE opciones SET activado = 1 WHERE id = 4",$conexion);
							}else{
								$resp = mysql_query("UPDATE opciones SET activado = 0 WHERE id = 4",$conexion);
							}

						}
						$resp = mysql_query("SELECT opciones.activado from opciones WHERE id = 1",$conexion);
						if($dato = mysql_fetch_array($resp)){
							if($dato[0] == 1){
								echo '<div id="validacionOk">La inscripción está activada.</div>';
							}else{
								echo '<div id="validacionAlert">La inscripción está desactivada.</div>';
							}
						}
						$resp = mysql_query("SELECT opciones.activado from opciones WHERE id = 2",$conexion);
						if($dato2 = mysql_fetch_array($resp)){
							if($dato2[0] == 1){
								echo '<div id="validacionOk">La matriculación está activada.</div>';
							}else{
								echo '<div id="validacionAlert">La matriculación está desactivada.</div>';
							}
						}
						$resp = mysql_query("SELECT opciones.activado from opciones WHERE id = 3",$conexion);
						if($dato3 = mysql_fetch_array($resp)){
							if($dato3[0] == 1){
								echo '<div id="validacionOk">La Inscripcion a Exámenes está activada.</div>';
							}else{
								echo '<div id="validacionAlert">La Inscripcion a Exámenes está desactivada.</div>';
							}
						}
						$resp = mysql_query("SELECT opciones.activado from opciones WHERE id = 4",$conexion);
						if($dato4 = mysql_fetch_array($resp)){
							if($dato4[0] == 1){
								echo '<div id="validacionOk">La Planilla de alumno regular está activada.</div>';
							}else{
								echo '<div id="validacionAlert">La Planilla de alumno regular está desactivada.</div>';
							}
						}

						?>
						<form action='?' method='GET'>
							<input type="hidden" name="opciones" value='1'>
							<label>¿Activar Inscripción? <input type="checkbox" name="inscripcion" value="1" <?php echo estadoInput($dato[0],1,'checkbox'); ?> /></label>
							<label>¿Activar Matriculación? <input type="checkbox" name="matriculacion" value="1" <?php echo estadoInput($dato2[0],1,'checkbox'); ?> /></label>
							<label>¿Activar Inscripción Exámenes? <input type="checkbox" name="examenes" value="1" <?php echo estadoInput($dato3[0],1,'checkbox'); ?> /></label>
							<label>¿Activar Planilla de alumno regular? <input type="checkbox" name="alumnoRegular" value="1" <?php echo estadoInput($dato4[0],1,'checkbox'); ?> /></label>


							<input type="submit" value="Aplicar" />
						</form>
						<?php
						echo "<li><a href='/adm.php?accion=salirAdmin'>Desloguearse</a></li>";

						echo "</ul></div>";

						echo '<h2>GUIA DEL ADMINISTRADOR:</h2>';
						echo '
							<h3>Agregar una nueva sección</h3>
							<ul class="listaEstilo">
							<li>Una vez ingresado al administrador, ingresar en la barra de direccion (URL) donde dice <b>www.empa.edu.ar/</b> reemplazarla toda por: <strong>http://www.empa.edu.ar/seccion/<em>nueva-seccion</em></strong> en donde <strong><em>nueva-seccion</em></strong> es el nombre de la nueva seccion que quieras poner. Presionar enter para entrar a esa página</li>
							<li>Una vez ingresado con la URL anterior, aparecera la página sin contenido pero con el editor de contenidos, ahi se debe escribir el contenido que se quisiera mostrar en esa seccion.</li>
							<li>Una vez guardado el contenido, Se debería crear un link en alguna otra seccion de la pagina para que se pueda acceder, por Ejemplo si queres agregar el link en la página principal hay que ir a <strong>empa.edu.ar</strong> y agregar el link con el editor de contenido seleccionando un texto y clickeando el icono de <strong>insertar/editar vinculo</strong> y ingresando en <strong>URL</strong> la direccion <strong>http://www.empa.edu.ar/seccion/<em>nueva-seccion</em></strong> en donde <strong><em>nueva-seccion</em></strong> era el nombre que habiamos elegido para la seccion.</li>
							</ul>

							<h3>Agregar archivos de descarga</h3>
							<ul class="listaEstilo">
							<li>Subir el archivo a la web en la carpeta <strong>descargas</strong> <em>recordar el nombre del archivo con el que se subió.</em></li>
							<li>Ir a la seccion que queramos insertar un link para descargar, en el editor de contenidos agregamos un link y en la dirección <strong>URL</strong> hay que poner http://www.empa.edu.ar/descargas/<em>nombre-del-archivo-subido</em>.</li>
							</ul>

							<h3>Agregar Imagen al contenido</h3>
							<ul class="listaEstilo">
							<li>Subir la imagen a la web en la carpeta <strong>img</strong> <em>recordar el nombre del archivo con el que se subió.</em></li>
							<li>Ir a la seccion que queramos insertar la imagen para mostrar, en el editor de contenidos agregamos una imagen y en la dirección <strong>URL</strong> que nos pide hay que poner http://www.empa.edu.ar/img/<em>nombre-de-la-imagen-subida</em>.</li>
							</ul>
						';
					}else{
						echo '<div id="validacionAlert">';
							echo 'Contraseña y/o usuario incorrectos!.';
						echo '</div>';

					
				?>
				<h2>Administrar:</h2>
				<form action="?" method="post">
					<label>Usuario: </label><input type="text" name="usuario" />
					<label>Contraseña:</label><input type="password" name="clave" />
					<input type="submit" value="Ingresar" />
				</form>
				<?php
					}
				?>
			</div>


			<!--<script type="text/javascript" src="js/jquery-1.6.2.js"></script>
			<script type="text/javascript" src="js/dinamico.js"></script>-->
		</div>
		</div>
	</body>
</html>

