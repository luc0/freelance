<footer>
	<ul>
		<li>Sede: Belgrano 581-Avellaneda</li>
		<li>Telefono - Fax 4222-6781</li>
		<li>E-mail: empa@empa.edu.ar</li>
	</ul>
	<ul>
		<li>Anexo: Av. Mitre 292 | 4201-4091</li>
		<li>Desarrollo web: <a href="http://www.lucianoperez.com.ar" title="Luciano Pérez Diseño y Programación Web">Luciano Pérez</a></li>
	</ul>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="js/dinamico.js?v=2mar"></script>
<?php 

//agrega JS depende la pagina
if(strstr($_SERVER['SCRIPT_NAME'],'ingreso')){
	echo '<script type="text/javascript" src="js/validacionIngreso.js?v=2mar"></script>';
	srcTooltip();
}
if(strstr($_SERVER['SCRIPT_NAME'],'matriculacion')){
	echo '<script type="text/javascript" src="js/validacionMatriculacion.js?v=2mar"></script>';
	srcTooltip();
}
if(strstr($_SERVER['SCRIPT_NAME'],'inscripcionExamenes')){
	echo '<script type="text/javascript" src="js/validacionInscripcionExamenes.js?v=2mar"></script>';
	srcTooltip();
}
if(strstr($_SERVER['SCRIPT_NAME'],'inscripcionExamenes')){
	echo '<script type="text/javascript" src="js/validacionAlumnoReg.js?v=27mar"></script>';
	srcTooltip();
}

function srcTooltip(){
	echo '
	<link rel="stylesheet" href="css/jquery.smallipop.css" type="text/css" media="all" title="Screen"/>
	<script type="text/javascript" src="js/tooltip/modernizr.js"></script>
	<script type="text/javascript" src="js/tooltip/jquery.smallipop.js"></script>
	<script type="text/javascript">
		$(".tooltip").smallipop({
		    preferredPosition: "right",
		    theme: "black",
		    popupOffset: 0,
		    triggerOnClick: true
		});
	</script>
	';
}

?>

<!-- Start of StatCounter Code-->
	<script type="text/javascript">
	var sc_project=7331583; 
	var sc_invisible=1; 
	var sc_security="2433227a"; 
	</script>
	<script type="text/javascript"
	src="http://www.statcounter.com/counter/counter.js"></script>
	<noscript><div class="statcounter"><a title="click
	tracking" href="http://statcounter.com/"
	target="_blank"><img class="statcounter"
	src="http://c.statcounter.com/7331583/0/2433227a/1/"
	alt="click tracking"></a></div></noscript>
<!-- End of StatCounter Code-->

<!--google Analytics-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12954596-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--End google Analytics-->
