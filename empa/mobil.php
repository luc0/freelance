<?php require_once("conexion.php");session_start();include_once("funciones.php");
$admin = $_SESSION[md5('adminSesion')];
	require_once('inyeccion.php');
	//soluciona problemas cuando se traen datos con caracteres raros de la bd
	@mysql_query("SET NAMES 'utf-8'"); 
	//require_once('inyeccion.php');
?>
<!DOCTYPE html> 
<html lang="es"> 
	<head>
		<title>EMPA</title>
		<link type="text/css" rel="stylesheet" href="css/mobil.css?v=1.0"/> <!--http://www.empa.edu.ar/-->
		<meta charset=utf-8 />
		<link rel="Shortcut Icon" href="/favicon.ico">
 		<meta name="description" content="Primera escuela Sudámericana de Musica popular de avellaneda. Formación básica, formación docente, profesorado de instrumento, Areas de jazz, tango y folklore"/>
		<meta name="keywords" content="escuela,musica,popular,carreras,instrumentista,tango,jazz,folklore,instrumento,avellaneda,musico" /> 
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]--> 
	</head>
	<body>
		<div id="contenedor">		
			<header>
				<h1><a href="/" title="Escuela de Música Popular">Empa</a></h1>
			</header>
			<aside>
				<ul>
					<li><a href="/seccion/calendarioEscolar" title="ver calendario escolar">Calendario Escolar</a></li>
					<li><a href="/seccion/pautasAcreditacion" title="ver pautas">Pautas de Acreditación</a></li>
					<li><a href="/seccion/historia" title="ver historia">Historia</a></li>
					<li><a href="/seccion/docentes" title="ver docentes">Docentes</a></li>
					<li><a href="/seccion/biblioteca" title="ver biblioteca">Biblioteca</a></li>
					<li><a href="/seccion/vinculos" title="ver vinculos">Vinculos</a></li>
				</ul>
			</aside>
			<section>
				<?php
				//$urlActual = basename($_SERVER['REQUEST_URI']);
				$seccion = $_GET['seccion'];
				//muestra contenido segun GET, si esta vacio pone contenido de INDEX
				contenido();
				?>
			</section>
			<nav>
				<ul>
					<li><a class="gris nofloat" href="?" title="IMP: 1º año y perspectivas comunes a los 3 géneros">IMP comunes</a>
					<ul>
						<li><a href="/seccion/ImpObjetivos" title="Objetivos">Objetivos Generales</a></li>
						<li><a href="/seccion/ImpContenidos" title="Contenidos Generales">Contenidos Generales</a></li>
						<li><a href="/seccion/ImpPerspectiva" title="Perspectiva">Perspectiva</a></li>
						<li><a href="/seccion/ImpDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/ImpCuadernillos" title="Cuadernillos">Listado de cuadernillos</a></li>
						<li><a href="/seccion/nivelSuperiorPlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/ImpHorarios" title="horarios">Horarios</a></li>
					</ul>
					</li>
					<li><a class ='violeta' href="?" title="profesorado de Instrumento">Profesorado de instrumento</a>
					<ul>
						<li><a href="/seccion/profObjetivos" title="Objetivos Generales">Objetivos Generales</a></li>
						<li><a href="/seccion/profContenidos" title="Contenidos Generales">Contenidos Generales</a></li>
						<li><a href="/seccion/profPerspectiva" title="Perspectiva">Perspectiva</a></li>
						<li><a href="/seccion/profDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/profCuadernillos" title="Cuadernillos">Listado de cuadernillos</a></li>
						<li><a href="/seccion/profPlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/profHorarios" title="horarios">Horarios</a></li>

					</ul>
					
					</li>
					<li><a class="naranja" href="?" title="formación Docente">Formación Docente</a>
					<ul>
						<li><a href="/seccion/formDocentePrincipios" title="Principios">Principios Institucionales</a></li>
						<li><a href="/seccion/formDocenteMetodologia" title="En Construccion...">Propuesta metodológica</a></li>
						<li><a href="/seccion/formDocentePautas" title="Pautas de acreditación">Pautas generales de acreditación</a></li>
						<li><a href="/seccion/formDocenteDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/formDocenteConvenios" title="Convenio">Convenios con otras insituciones</a></li>
						<li><a href="/seccion/formDocentePlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/formHorarios" title="horarios">Horarios</a></li>
					</ul>

					</li>
					<li><a class="azul" href="?" title="formación Basica">Formación Básica</a>
					<ul>
						<li><a href="/seccion/formBasicaObjetivos" title="Objetivos">Objetivos Generales</a></li>
						<li><a href="/seccion/formBasicaMetodologia" title="metodologia">Metodologia de Trabajo</a></li>
						<li><a href="/seccion/pautasAcreditacion" title="Pautas de acreditación">Pautas de Acreditación</a></li>
						<li><a href="/seccion/formBasicaDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/formBasicaCuadernillos" title="Cuadernillos">Listado de cuadernillos</a></li>
						<li><a href="/seccion/formBasicaPlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/formBasicaHorarios" title="horarios">Horarios</a></li>
					</ul>

					</li>
					<li><a class="amarillo" href="?" title="folklore">Área Folklore</a>
					<ul>
						<li><a href="/seccion/folkloreObjetivos" title="Objetivos">Objetivos Generales</a></li>
						<li><a href="/seccion/folklorePerspectiva" title="Perspectiva">Perspectiva</a></li>
						<li><a href="/seccion/folkloreDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/folkloreCuadernillos" title="Cuadernillos">Listado de cuadernillos</a></li>
						<li><a href="/seccion/nivelSuperiorPlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/folkloreHorarios" title="horarios">Horarios</a></li>
					</ul>

					</li>
					<li><a class="verde" href="?" title="tango">Área Tango</a>
					<ul>
						<li><a href="/seccion/tangoObjetivos" title="Objetivos">Objetivos Generales</a></li>
						<li><a href="/seccion/tangoPerspectiva" title="Perspectiva">Perspectiva</a></li>
						<li><a href="/seccion/tangoDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/tangoCuadernillos" title="Cuadernillos">Listado de cuadernillos</a></li>
						<li><a href="/seccion/nivelSuperiorPlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/tangoHorarios" title="horarios">Horarios</a></li>
					</ul>
					</li>
					<li><a class="rojo" href="?" title="jazz">Área Jazz</a>
					<ul>
						<li><a href="/seccion/jazzObjetivos" title="Objetivos">Objetivos Generales</a></li>
						<li><a href="/seccion/jazzContenidos" title="Contenidos Generales">Contenidos Generales</a></li>
						<li><a href="/seccion/jazzPerspectiva" title="Perspectiva">Perspectiva</a></li>
						<li><a href="/seccion/jazzDocentes" title="Docentes">Docentes</a></li>
						<li><a href="/seccion/jazzCuadernillos" title="Cuadernillos">Listado de cuadernillos</a></li>
						<li><a href="/seccion/nivelSuperiorPlanEstudio" title="plan de estudios">Plan de estudios</a></li>
						<li><a href="/seccion/jazzHorarios" title="horarios">Horarios</a></li>
					</ul>
					</li>
					<!--<li><a class="gris" href="index.php" title="inicio">Perspectivas Comunes a los tres Géneros</a></li>-->
				</ul>	
			</nav>
		</div>
	</body>
</html>

