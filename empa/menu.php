<aside>
	<div id="menuVertical">
	<ul>
		<li><a href="/" title="ir a inicio">Principal</a></li>
		<!--<li><span>Carreras</span></li>-->
		<li><a href="/seccion/calendarioEscolar" title="ver calendario escolar">Calendario Escolar</a></li>
		<li><a href="/seccion/pautasAcreditacion" title="ver pautas">Pautas de Acreditación</a></li>
		<!--<li><a href="index.php?seccion=equipoConduccion" title="ver equipo de conduccion">Equipo de conducción</a></li>-->
		<li><a href="/seccion/historia" title="ver historia">Historia</a></li>
		<li><a href="/seccion/docentes" title="ver docentes">Docentes</a></li>
		<!--<li><span>Alumnos</span></li>-->
		<li><a href="/seccion/extensionCultural" title="extension cultural">Extensión Cultural</a></li>
		<li><a href="/seccion/biblioteca" title="ver biblioteca">Biblioteca</a></li>
		<li><a href="/seccion/infoGeneral" title="ver informacion general">Información General</a></li>
		<li><a href="/seccion/vinculos" title="ver vinculos">Vinculos</a></li>

	</ul>
	</div>
	<?php
		if(!empty($_SESSION[md5('adminSesion')])){
			echo "<div id='asideAdmin'><h3>Administrador</h3><ul>";
			echo "<li><a href='adm.php?accion=verIngresantes&p=1'>Ver todos los ingresantes</a></li>";
			echo "<li><a href='adm.php?accion=salirAdmin'>Desloguearse</a></li></ul></div>";		
		}
	?>

</aside>

