<? require_once("conexion.php");/*session_start();include("funciones.php");*/
		include_once('cabecera.php'); //CABECERA
?>
<div id="contenedor" >		
	<section id="contenido">

		<h2>Preguntas Frecuentes</h2>

		<h3>¿De quién depende la Escuela?</h3>

		Depende de la Dirección de Educación Artística de la Dirección General de Cultura y Educación de la Provincia de Buenos Aires

		<h3>¿La Escuela es gratuita?</h3>

		Si

		<h3>¿Qué nivel de enseñanza tiene?</h3>

		Terciario no universitario

		<h3>¿Qué carreras se cursan?</h3>
		<ul>
			<li>Instrumentista en Música Popular especializado en Tango, en Jazz o en Folclore</li>
			<li>Profesor de Música</li>
			<li>Profesorado de Instrumento</li>
		</ul>
		<h3>¿Hay cursos breves o talleres?</h3>

		NO, en la Escuela sólo se dictan carreras.

		<h3>¿Qué títulos otorga la Escuela?</h3>
		<ul>
			<li>Instrumentista en Música Popular</li>
			<li>Profesor de Artes en Música para los niveles inicial y EGB 1 y 2</li>
			<li>Profesor de Artes en Música para EGB 3 y Polimodal</li>
			<li>Profesor de Instrumento en música Popular</li>
		</ul>
		<h3>¿Para qué habilita el título de profesor de Artes en Música?</h3>

		Los títulos del Profesorado de Artes en Música te habilitan para la enseñanza de Música en los niveles inicial, primaria y secundaria, indicados en título tanto en la educación Pública como Privada. El profesorado de instrumento HASTA EL MOMENTO tiene la misma competencia, pero con menor puntaje en el nomenclador de títulos.

		<h3>¿El título de Instrumentista en Música Popular me habilita para ejercer la docencia?</h3>

		no tiene validez docente

		<h3>¿Qué instrumentos se pueden cursar en la Escuela?</h3>

		Acordeón, Aerófonos Autóctonos, Armónica, Bajo eléctrico, Bandoneón, Batería, Canto, Charango, Clarinete, Contrabajo, Flauta Traversa, Guitarra, Piano, Saxo, Trombón, Trompeta, Violín, Violoncello, Percusión.

		<h2>SOBRE EL INGRESO</h2>

		<h3>¿Qué nivel debo colocar en la ficha de inscripción?</h3>

		El nivel <b>AL QUE ASPIRO INGRESAR </b>

		<h3>¿Si luego de haber enviado el formulario de inscripción cambio de idea sobre el nivel o el instrumento al cual me inscribí, que puedo hacer?</h3>

		La segunda instancia, ó sea la confirmación de la inscripción, en febrero, es el momento definitivo de elegir instrumento, nivel o carrera a la cual ingresar

		<h3>¿Cuál es la edad mínima para ingresar?</h3>

		15 años

		<h3>¿Cuál es la edad máxima para ingresar?</h3>

		No hay tope máximo de edad

		<h3>¿Puedo ingresar si no tengo la Secundaria terminada?</h3>

		Sólo a la Formación Básica. Para ingresar al Ciclo Superior es necesario tener los estudios secundarios completos

		<h3>¿Puedo ingresar si no tengo la Educación Primaria Básica terminada?</h3>

		No, ya que es un establecimiento de Nivel Terciario

		<h3>¿Puedo ingresar si soy extranjero?</h3>

		Si, solo debe realizar los trámites pertinentes ante Migraciones. La escuela tramita el certificado electronico de residencia.

		<h3>¿Qué documentación debo presentar para ingresar?</h3>

		Para concretar esta Instancia deberán concurrir con la siguiente Documentación:
		<ul>
			<li>Recibo de la Inscripción On Line (Número de inscripción Obligatorio)</li>
			<li>Carpeta de cartulina 3 solapas color amarillo</li>
			<li>Fotocopia D.N.I.; (Pasaporte o documentación que acredite identidad en el caso de ser extranjero)</li>
			<li><b>TITULO SECUNDARIO</b>  o  Constancia de alumno regular (En caso de estar haciendo el secundario)</li>
			<li>Certificado Psicofísico, firmado por un médico matriculado. DESCARGAR FICHA MEDICA</li>
			<li>¿Desde algún otro conservatorio puedo ingresar por pase?</li>
		</ul>
		Solamente desde la carrera de Instrumentista en Música Popular del la Escuela de Arte Leopoldo Marechal. Si provenís de otro conservatorio ingresarías mediante prueba de nivel.

		<h3>¿Si no se nada de música, puedo ingresar?</h3>

		SI, te inscribirías para realizar el Nivel Preparatorio

		En caso de no poder concurrir personalmente en febrero para la segunda instancia (Confirmación de inscripción), Existe la posibilidad de que vaya otra persona? O es imprescindible que sea el inscripto quien vaya?

		Podes enviar a un representante que sepa exactamente los datos que se requieren para efectuar la confirmación (Fundamentalmente Instrumento y Nivel al que aspira rendir la prueba de nivelación)

		  

		<h2>SOBRE LA PRUEBA DE NIVEL</h2>

		<h3>¿A qué nivel puedo ingresar mediante prueba de nivel?</h3>

		A 1º, 2º ó 3º nivel de Formación Básica ó a 1º año del Ciclo Superior de las carreras de Instrumentista o de Formación Docente

		<h3>¿Qué debo rendir en la prueba de nivel?</h3>

		Los contenidos del programa de Lenguaje Musical y de Instrumento del nivel inmediato anterior al que deseo ingresar (Por Ej: para ingresar al Nivel III de Formación Básica se deberán rendir los contenidos de Lenguaje Musical II e Instrumento del Nivel II)

		<h3>¿Puedo rendir un nivel de Lenguaje Musical y uno distinto de Instrumento?</h3>

		NO. Para la Prueba de nivelación deben ser los mismos los niveles de <b>LENGUAJE MUSICAL</b> y de <b>INSTRUMENTO</b>.

		<h3>¿Qué sucede si no me presento a alguna de las dos pruebas? (Lenguaje Musical ó Instrumento)</h3>

		Pierdo el derecho de ingreso

		<h3>¿Tengo que llevar mi instrumento para la prueba de nivel?</h3>

		Si, excepto piano, contrabajo y batería (Traer Platillos y palillos).

		 

		<h2>SOBRE LAS CURSADAS</h2>

		<h3>¿Qué carga horaria significa cursar en la escuela?</h3>

		Depende del año de cursada. Puede ser de 5 hs semanales para los alumnos del Curso Preparatorio ó 1º Nivel de la Formación Básica; hasta 15 hs., aproximadamente por semana (para los alumnos del 1º año del ciclo superior).

		<h3>¿En que horarios se cursa en la Escuela?</h3>

		La Escuela funciona de lunes a viernes de 8 a 23 hs y los sábados de 9 a 17 hs.

		<h3>¿Puedo elegir los horarios para cursar?</h3>

		En la instancia de la Matriculación se realiza la elección de horarios

		<h3>¿Tengo que elegir todos los horarios dentro del mismo Turno?</h3>

		No necesariamente.

		<h3>¿Tengo alguna prioridad para elegir horarios por incompatibilidad horaria con mi trabajo?</h3>

		Si, presentando certificado de trabajo

		<h3>¿Cuál es el régimen de asistencia de las cursadas?</h3>

		Hay que cumplir, en cada materia, con un 80% de asistencia

		<h3>¿Todas las materias son anuales?</h3>

		Si

		<h3>¿Puedo cursar las materias de un mismo nivel en más de un ciclo lectivo?</h3>

		Si, respetando la correlatividad

		<h3>¿Cómo se aprueba una cursada?</h3>

		Hay que aprobar dos parciales en el año. Si la calificación promedio de dichos parciales es 7 (siete) ó mas la asignatura queda aprobada <b>“POR PROMOCION”</b>.  Si la calificación del segundo parcial es inferior a 7 ó si el promedio es una calificación entre 4 (cuatro) y 7 (siete) se deberá rendir final para la aprobación de la materia.

		<h3>¿Qué materias se rinden con exámen final?</h3>

		De instrumento (obligatorio) y de aquellas materias que no se hallan <b>“PROMOCIONADO”</b>

		<h3>¿Se puede cursar on-line?</h3>

		No

		<h3>¿Puedo cursar solo algunas materias, como por ejemplo Instrumento?</h3>

		No. Para pasar de un año al siguiente hay que respetar un régimen de correlatividades. (Ver Plan de Estudios)

		<h3>¿Se puede rendir exámen LIBRE?</h3>

		Sólo de asignaturas de la Formación Básica (Excepto de Práctica de Conjunto y Práctica Coral que son de cursada obligatoria)

		<h3>¿Se reconocen materias aprobadas en otro conservatorio?</h3>

		Todos los años hay un período de presentación de <b>EQUIVALENCIAS</b>, generalmente en el mes de mayo. La aprobación de las mismas depende de la evaluación que el docente a cargo de la asignatura haga de los contenidos que el alumno presente en el programa de la asignatura aprobada en el establecimiento de origen.

		<h3>¿Puedo cursar dos carreras o dos instrumentos?</h3>

		No

		<h3>¿Cuántos años duran las carreras?</h3>
		<ul>
			<li>La Formación Básica dura 3 niveles de aproximadamente un año cada uno</li>
			<li>La carrera de Instrumentista en Música Popular dura 4 años (Es necesario tener aprobada la Formación Básica)</li>
			<li>La carrera de Profesor de Artes en Música para Educación Inicial y EGB 1 y 2 dura 3 años (Es necesario tener aprobada la Formación Básica)</li>
			<li>La carrera de Profesor de Artes en Música para EGB 3 y Polimodal dura 1 año (Es necesario tener aprobada la carrera de Profesor de Artes en Música para la Educación Inicial y EGB 1 y 2)</li>
			<li>La carrera de Profesor de Instrumento en Música Popular dura 4 años (Es necesario tener aprobada la Formación Básica)</li>
		</ul>
		<p class='atencion'>“SI TE QUEDAN DUDAS ENVIANOS TUS INQUIETUDES A info@empa.edu.ar”</p>

	</section>
		<?php
			include_once('menu.php'); //MENU
			include_once('pie.php'); //PIE
		?>
		</div>
		</div>
	</body>
</html>


