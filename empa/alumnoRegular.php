<? require_once("conexion.php");session_start();/*include("funciones.php");*/
		$admin = $_SESSION[md5('adminSesion')];
		include_once('cabecera.php'); //CABECERA
		require_once("funciones.php");

	if(!empty($_POST)){
		echo '<div id="contenedor"><section>';
		echo '<div id="validacionAlert" class="espera">';
		echo 'Enviando Email... espere unos segundos';
		echo '</div>';
		require_once("clases/class.validacion.php");
		$validacion = new Validacion('algo','algo','alfanumerico','algo','algo'); // <---- tipo
		$camposErroneos = $validacion->campos('nombre','apellido','dni','nivel','area'); // <---- name de los input
		$camposErroneos = limpiarArray($camposErroneos);
		if(count($camposErroneos)){
			echo '<div id="validacionMsg">';
				echo 'Verifique los siguientes campos: ';
				foreach ($camposErroneos as $indice=>$valor){
					echo '<b>'.$camposErroneos[$indice].'</b> ';
				}
			echo '</div>';
		}else{
			///////////////////////////////////////////////
			$mesNombre = array('Enero','Febrero','Marzo','Abril','Marzo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
			$web = "www.empa.edu.ar"; //nombre de la web
			$nombre = htmlspecialchars($_POST['nombre'], ENT_QUOTES);
			$apellido = htmlspecialchars($_POST['apellido'], ENT_QUOTES);
			$fecha = date('d/m/Y');
			$dni = $_POST['dni'];
			$nivel = $_POST['nivel'];
			$area = $_POST['area'];

			if($area == 'FOBA'){
				$destinatario = 'preceptoriafoba@empa.edu.ar';
			}elseif($area == 'formacion_docente' || $area == 'PIMP'){
				$destinatario = 'preceptoriaprofesorado@empa.edu.ar';
			}elseif($area == 'IMP' || $area == 'folklore' || $area == 'tango' || $area == 'jazz'){
				$destinatario = 'preceptoriasuperior@empa.edu.ar';
			}

			$fArr = explode('/',$fecha);
			$dia = date('d');
			$mesNumero = date('n');
			$mes = $mesNombre[$mesNumero-1];
			$ano = date('Y');
			$carrera = 'Instrumentista';//cambiar

			$remitente = "empa@empa.edu.ar";

			///////////////////////////////////////////////

			$headers = "Content-type: text/html; charset=utf-8 
			From: $remitente
			Return-path: $remitente";
			$cuerpo = file_get_contents("vistas/mailAlumnoRegular.php");
			$cuerpo = str_replace('[nombre]',$nombre,$cuerpo);
			$cuerpo = str_replace('[apellido]',$apellido,$cuerpo);
			$cuerpo = str_replace('[dni]',$dni,$cuerpo);
			$cuerpo = str_replace('[fecha]',$fecha,$cuerpo);
			$cuerpo = str_replace('[nivel]',$nivel,$cuerpo);
			$cuerpo = str_replace('[area]',$area,$cuerpo);
			$cuerpo = str_replace('[carrera]',$carrera,$cuerpo);
			$cuerpo = str_replace('[dias]',$dia,$cuerpo);
			$cuerpo = str_replace('[mes]',$mes,$cuerpo);
			$cuerpo = str_replace('[ano]',$ano,$cuerpo);
			$cuerpo = str_replace('\\','',$cuerpo);
			$nombre = str_replace('\\','',$nombre);
			$apellido = str_replace('\\','',$apellido);

			if(mail($destinatario,"$web: Mensaje de $nombre $apellido",$cuerpo,$headers)){
				echo '
				<div id="validacionOk">
					La información se envió correctamente!.
				</div>
				';
			}else{
				echo '
				<div id="validacionAlert">
					Lo siento! no se pudieron enviar los datos! <a href="/alumnoRegular.php">intente nuevamente</a>, o mas tarde.
				</div>
				';
			}
			?>
			<script>
				element = document.getElementById("validacionAlert");
				element.parentNode.removeChild(element);
			</script>
			</section></div><!--cierra contenedor y section-->
			<?php

			include_once('menu.php'); //MENU
			include_once('pie.php'); //PIE
		}
	}else{
		mysql_select_db($bd_ingresantes,$conexion);
		$resp = mysql_query("SELECT opciones.activado from opciones WHERE id = 4",$conexion);
		if($dato = mysql_fetch_array($resp)){
			if($dato[0] == 1){
			/*PLANILLA ALUMNO REGULAR ESTA HABILITADA!*/
?>

		
		<div id="contenedor" >		
			<section id="contenido">
				<h2>Constancia de alumno regular</h2>
				<form method='post' action='?' class="alumnoReg">
					<label>Nombre</label><input type="text" name="nombre" maxlength="40" value="<?php echo $_POST['nombre']; ?>"/>
					<label>Apellido</label><input type="text" name="apellido" maxlength="40" value="<?php echo $_POST['apellido']; ?>"/>
					<label>Dni</label><input type='text' name="dni" maxlength="35"/>
					<label>Area</label>
					<select name="area" class="areaSelectAlumnoReg">
						<option value="">-SELECCIONE-</option>
						<option value="FOBA">Formación básica</option>
						<option value="IMP">1º Año y Materias Comunes (IMP)</option> 
						<option value="folklore">Folklore</option>
						<option value="tango">Tango</option>
						<option value="jazz">Jazz</option>
						<option value="PIMP">Profesorado de Instrumento (PIMP)</option>
						<option value="formacion_docente">Formación Docente</option>
					</select>
					<label>Nivel</label>
					<select name="nivel" class="nivelSelect">
							<option value=""'.estadoInput($_POST["nivel"],0,"select").'>-SELECCIONE-</option>
					</select>
					<input type="submit" value="Enviar">
				</form>
			</section>
		<?php
			include_once('menu.php'); //MENU
			include_once('pie.php'); //PIE
		?>
		</div>
		</div>
		<script type="text/javascript" src="js/validacionAlumnoReg.js"></script>
<?php

			}else{//cierre if si esta habilitado el form
				echo '
					<div id="contenedor">		
						<section id="contenido">
							<h2>El formulario de Alumno regular esta deshabilitado por el momento.</h2>
						</section>
					</div>
				';
				include_once('menu.php'); //MENU
				include_once('pie.php'); //PIE
			}
		}
	}
		
?>
	</body>
</html>



