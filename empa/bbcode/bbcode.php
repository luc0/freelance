<?php 

$bb_code = array(  
//Letra negrita 
'[b]' => '<strong>',  //La parte de [b] es en BBcode 
'[/b]' => '</strong>', //La parte de <strong> es lo que PHP va a remplazar y así en todos los casos

//Letra cursiva 
'[i]' => '<em>',  
'[/i]' => '</em>',  

//Letra subrayada 
'[u]' => '<u>',  
'[/u]' => '</u>',  

//Enter 
'/n' => '<br>',  

//Imagenes 
'[img]' => '<img src="',  
'[/img]' => '" />' , 

//Titulos
'[h1]' => '<h1>',
'[/h1]' => '</h1>',
'[h2]' => '<h2>',
'[/h2]' => '</h2>',
'[h3]' => '<h3>',
'[/h3]' => '</h3>',
'[h4]' => '<h4>',
'[/h4]' => '</h4>',
'[h5]' => '<h5>',
'[/h5]' => '</h5>',
'[h6]' => '<h6>',
'[/h6]' => '</h6>',

//Links 
'[/url]' => '</a>',
']' => '">', 
'[url=' => '<a href="'
); 

function reemplazar($codigo) {  
	$search = array_keys( $GLOBALS['bb_code'] );  
	$codigo = str_replace( $search, $GLOBALS['bb_code'], $codigo );  
	return $codigo;  
}
// Definimos nuestro mensaje de prueba  
$mensaje = ' 
Esta palabra está en [b]negrita[/b] y [u]esto está subrayado.[/u] 
Esto es un URL 
[url=http://www.google.com]link[/url]
Esta es una imágen 
[img]http://www.trescirculos.com/avatar/comunidad/index.php[/img]';  

// Reemplazamos el BBCode por código HTML y lo mostramos en la página  
echo '<p>'.reemplazar( $mensaje ).'</p>';  

?>
