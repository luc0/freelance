			<form method="post" action="?">
					<h3>Datos Personales.</h3>
					<label>Nombre</label><input type="text" name="nombre" maxlength="40" value="<?php echo $_POST['nombre']; ?>"/>
					<label>Apellido</label><input type="text" name="apellido" maxlength="40" value="<?php echo $_POST['apellido']; ?>"/>
					<div>
						<div><label>Documento</label></div>
						<label>Tipo</label>
						<select name="dni_tipo">
							<option value="dni" <?php echo estadoInput($_POST['dni(tipo)'],'dni','select'); ?>>DNI</option>
							<!--<option value="lc" <?php echo estadoInput($_POST['dni(tipo)'],'lc','select'); ?>>LC</option>-->
							<option value=" " <?php echo estadoInput($_POST['dni(tipo)'],' ','select'); ?>>Otro..</option>
						</select>
						<div><label>Nº </label> <input type="text" name="documento" maxlength="45" value="<?php echo $_POST['documento']; ?>"/></div>
					</div>
					<label>Instrumento</label>
					<select name="instrumento">
						<option value="">-SELECCIONE-</option>
						<?php
							mysql_select_db($bd_ingresantes,$conexion);
							$resp = mysql_query("SELECT * from instrumentos WHERE activo = 1 ORDER BY valor",$conexion);
							while($datos = mysql_fetch_array($resp)){
				echo '<option value="'.$datos['id'].'" '.estadoInput($_POST['instrumento'],1,'select').'>'.$datos['valor'].'</option>';
							}
						?>
						
					</select>

					<label>Email</label><input type="text" class="tooltip" name="email" maxlength="50" value="<?php echo $_POST['email']; ?>" title="Ingresa un mail válido ya que es donde se te enviará el link para imprimir el comprobante."/>
					<label>Teléfono</label><input type="text" name="telefono" maxlength="30" value="<?php echo $_POST['telefono']; ?>"/>
		



					<?php
					// 8 ESPACIOS PARA RELLENAR //
					for($i=1;$i<15;$i++){
						echo'
						<div class="division">

						<label>Area</label>
						<select class="areaSelectGeneral" name="area'.$i.'">
							<option value="">-SELECCIONE-</option>
							<option value="FOBA">Formación básica</option>
							<option value="IMP">1º Año y Materias Comunes (IMP)</option> 
							<option value="folklore">Folklore</option>
							<option value="tango">Tango</option>
							<option value="jazz">Jazz</option>
							<option value="PIMP">Profesorado de Instrumento (PIMP)</option>
							<option value="formacion_docente">Formación Docente</option>
						</select>

						<div><label>Asignatura</label>
						<select class="asignaturaSelect" name="asignatura'.$i.'">
				
					<option value="">-SELECCIONE-</option>
				<option value="acustica">acústica</option>
				<option value="análisis_de_textos">análisis de textos</option>
				<option value="bajo">bajo</option>
				<option value="canto">canto</option>
				<option value="Clarinete">Clarinete</option>
				<option value="canto_colectivo_I">canto colectivo I</option>
				<option value="canto_colectivo_II">canto colectivo II</option>
				<option value="Coord_de_conjuntos_vocales_e_instrumentales">Coord de conjuntos vocales e instrumentales</option>
				<option value="didactica_general">didáctica general</option>
				<option value="educación_vocal_I">educación vocal I</option>
				<option value="educación_vocal_II">educación vocal II</option>
				<option value="elementos_técnicos_de_la_música_I">elementos técnicos de la música I</option>
				<option value="elementos_técnicos_de_la_música_II">elementos técnicos de la música II</option>
				<option value="elementos_técnicos_de_la_música_III">elementos técnicos de la música III</option>
				<option value="elementos_técnicos_de_la_música_IV">elementos técnicos de la música IV</option>
				<option value="espacio_de_la_practica_docente_I">espacio de la práctica docente I</option>
				<option value="espacio_de_la_practica_docente_II">espacio de la práctica docente II</option>
				<option value="espacio_de_la_practica_docente_III">espacio de la práctica docente III</option>
				<option value="espacio_de_la_practica_docente_IV">espacio de la práctica docente IV</option>
				<option value="espacio_institucional">espacio institucional</option>
				<option value="folklore_musical_argentino">folklore musical argentino</option>
				<option value="Técnicos de Folklore III">Técnicos de Folklore III</option>
				<option value="fundamentos_de_la_educación">fundamentos de la educación</option>
				<option value="historia_de_la_música_I">historia de la música I</option>
				<option value="historia_de_la_música_II">historia de la música II</option>
				<option value="historia_de_la_música_III">historia de la música III</option>
				<option value="informática_aplicada_a_la_educación">informática aplicada a la educación</option>
				<option value="instrumento_armónico_guitarra">instrumento armónico guitarra</option>
				<option value=">instrumento_aromnico_piano">instrumento armónico piano</option>
				<!--<option value="lenguaje_musical_I_preparatorio_">lenguaje musical I (preparatorio)</option>-->
				<option value="lenguaje_musical_I">lenguaje musical I</option>
				<option value="lenguaje_musical_II">lenguaje musical II</option>
				<option value="lenguaje_musical_III">lenguaje musical III</option>
				<option value="medios_audiovisuales">medios audiovisuales</option>
				<option value="percusion">percusión</option>
				<option value="perspectiva_filosofica_pedagogica_didactica">perspectiva filosófica-pedagogica didactica </option>
				<option value="perspectiva_filosofico_pedagogica_I">perspectiva filosófico-pedagogica I</option>
				<option value="perspectiva_filosofico_pedagogica_II">perspectiva filosófico-pedagogica II</option>
				<option value="perspectiva_pedagogica_didactiva_I">perspectiva pedagogica-didactiva I</option>
				<option value="perspectiva_pedagogica_didactiva_II">perspectiva pedagogica-didactiva II</option>
				<option value="perspectiva_politico_institucional">perspectiva político institucional</option>
				<option value="perspectiva_socio_politica">perspectiva socio politica</option>
				<option value="practica_coral">práctica coral</option>
				<option value="practica_docente_I">práctica docente I</option>
				<option value="practica_docente_II">práctica docente II</option>
				<option value="Produccion_y_análisis_I">Producción y análisis I</option>
				<option value="Produccion_y_análisis_II">Producción y análisis II</option>
				<option value="Produccion_metodología">Producción metodología</option>
				<option value="proyecto II">proyecto II</option>
				<option value="psicologia_y_cultura_del_alumno_de_nivel_inicial_y_EGB_1_y_2">psicología y cultura del alumno de nivel inicial y EGB 1 y 2</option>
				<option value="psicología_y_cultura_de_alumnos_de _3ero">psicología y cultura de alumnos de 3º</option>
				<option value="taller_de_trabajo_corporal">taller de trabajo corporal</option>
				<option value="tecnicas_de_improvisacion_vocal_e_instrumental">técnicas de improvisación vocal e instrumental</option>
				<option value="teoría_de_la_percepción_y_la_comunicación">teoría de la percepción y la comunicación</option>
				<option value="historia_social_general">historia social general</option>
				<option value="historia_socio_politica_latinoamericana_y_argentina">historia socio política latinoamericana y argentina</option>
				<option value="acordeon">acordeón</option>
				<option value="apreciacion_del_folklore">apreciación del folklore</option>
				<option value="apreciacion_de_jazz">apreciación de jazz</option>
				<option value="apreciacion_musical">apreciación musical</option>
				<option value="apreciacion_tango">apreciación tango</option>
				<option value="armonica_foba">armónica foba</option>
				<option value="bajo_foba">bajo foba</option>
				<option value="bateria_foba">batería foba</option>
				<option value="cello_foba">cello foba</option>
				<option value="charango_foba">charango foba</option>
				<option value="contrabajo_foba">contrabajo foba</option>
				<option value="dicción_inglesa">dicción inglesa</option>
				<option value="diccion_portuguesa">diccion portuguesa</option>
				<option value="flauta_foba">flauta foba</option>
				<option value="foniatria">foniatría</option>
				<option value="guitarra_foba">guitarra foba</option>
				<option value="jefe_de_área_foba">jefe de área foba</option>
				<option value="percusión_de_manos">percusión de manos</option>
				<option value="piano">piano</option>
				<option value="practica_de_conjunto_voc_e_inst">práctica de conjunto voc e inst</option>
				<option value="repertorio">repertorio</option>
				<option value="saxo">saxo</option>
				<option value="taller_de_trabajo_corporal">taller de trabajo corporal</option>
				<option value="violin">violín</option>
				<option value="acordeon_aerofonos">acordeón aerófonos</option>
				<option value="bajo_folklore">bajo folklore</option>
				<option value="bandoneon_folklore">bandoneón folklore</option>
				<option value="canto_folklore">canto folklore</option>
				<option value="charango_folklore">charango folklore</option>
				<option value="contrabajo_folklore">contrabajo folklore</option>
				<option value="danzas_folkloricas_argentinas">danzas folklóricas argentinas</option>
				<option value="elementos_tecnicos_folklore I">elementos técnicos folklore I</option>
				<option value="elementos_tecnicos_folklore II">elementos técnicos folklore II</option>
				<option value="flauta_folklore">flauta folklore</option>
				<option value="guitarra_folklore">guitarra folklore</option>
				<option value="historia_folklore">historia folklore</option>
				<option value="orquesta_folklorica_I">orquesta folklórica I</option>
				<option value="orquesta_folkorica_II">orquesta folklórica II</option>
				<option value="percusion_complementaria">percusión complementaria</option>
				<option value="percusión_de_manos_folklore">percusión de manos folklore</option>
				<option value="percusion_folklore_I">percusión folklore I</option>
				<option value="percusion_folklore_II">percusión folklore II</option>
				<option value="percusion_folklore_III">percusión folklore III</option>
				<option value="percusion_folklore_IV">percusión folklore IV</option>
				<option value="piano_folklore">piano folklore</option>
				<option value="practica_grupal_andina">práctica grupal andina</option>
				<option value="practica_grupal_folklore_escrita_I">práctica grupal folklore (escrita) I</option>
				<option value="practica_grupal_folklore_escrita_II">práctica grupal folklore (escrita) II</option>
				<option value="practica_grupal_folklore_escrita_III">práctica grupal folklore (escrita) III</option>
				<option value="practica_grupal_folklore_espontanea_I">práctica grupal folklore (espontanea) I</option>
				<option value="practica_grupal_folklore_espontanea_II">práctica grupal folklore (espontanea) II</option>
				<option value="practica_grupal_folklore_espontanea_III">práctica grupal folklore (espontanea) III</option>
				<option value="violin_folklore">violín folklore</option>
				<option value="bajo_jazz">bajo jazz</option>
				<option value="bateria_jazz">batería jazz</option>
				<option value="canto_jazz">canto jazz</option>
				<option value="contrabajo_jazz">contrabajo jazz</option>
				<option value="elementos_tecnicos_jazz_I">elementos técnicos jazz I</option>
				<option value="elementos_tecnicos_jazz_II">elementos técnicos jazz II</option>
				<option value="elementos_tecnicos_jazz_III">elementos técnicos jazz III</option>
				<option value="flauta_jazz">flauta jazz</option>
				<option value="guitarra_jazz">guitarra jazz</option>
				<option value="historia_del_jazz_I">historia del jazz I</option>
				<option value="historia_del_jazz_II">historia del jazz II</option>
				<option value="orquesta_de_jazz">orquesta de jazz</option>
				<option value="piano_jazz">piano jazz</option>
				<option value="practica_grupal_jazz_I">práctica grupal jazz I</option>
				<option value="practica_grupal_jazz_II">práctica grupal jazz II</option>
				<option value="practica_grupal_jazz_III">práctica grupal jazz III</option>
				<option value="saxo_jazz">saxo jazz</option>
				<option value="trombon_jazz">trombón jazz</option>
				<option value="acustica_mcimp">acústica mcimp</option>
				<option value="arreglos_vocales_e_instrumentales">arreglos vocales e instrumentales</option>
				<option value="elementos_técnicos_de_la_música_I">elementos técnicos de la música I</option>
				<option value="entrenamiento_auditivo_I">entrenamiento auditivo I</option>
				<option value="entrenamiento_auditivo_II">entrenamiento Auditivo II</option>
				<option value="historia_de_la_música">historia de la música</option>
				<option value="medios_audiovisuales">medios audiovisuales</option>
				<option value="medios_electro_acústicos">medios electro acústicos</option>
				<option value="musica_latinoamericana_I">música latinoamericana I</option>
				<option value="musica_latinoamericana_II_practico">música latinoamericana II practico</option>
				<option value="musica_latinoamericana_teorica_II">música latinoamericana teorica II</option>
				<option value="practica_de_conjunto_vox_e_inst">práctica de conjunto vox e inst</option>
				<option value="practica_grupal">práctica grupal</option>
				<option value="taller_de_trabajo_corporal">taller de trabajo corporal</option>
				<option value="espacio_de_la_practica_docente_profesorado_I">espacio de la práctica docente (profesorado) I</option>
				<option value="espacio_de_la_practica_docente_profesorado_II">espacio de la práctica docente (profesorado) II</option>
				<option value="perspectiva_filosofico_pedagogica_I">perspectiva filosófico pedagogica I</option>
				<option value="perspectiva_filosofico_pedagogica_II">perspectiva filosófico pedagogica II</option>
				<option value="perspectiva_pedagogico_didactica_I">perspectiva pedagogico didactica I</option>
				<option value="perspectiva_pedagogico_didactica_II">perspectiva pedagogico didactica II</option>
				<option value="perspectiva_socio_politica">perspectiva socio politica</option>
				<option value="psicología_y_cultura_en_la_educación">psicología y cultura en la educación</option>
				<option value="psicologia_y_cultura_en_la_niñez">psicologia y cultura en la niñez</option>
				<option value="bajo_tango">bajo tango</option>
				<option value="bandoneon_tango">bandoneon tango</option>
				<option value="canto_tango">canto tango</option>
				<option value="contrabajo_tango">contrabajo tango</option>
				<option value="elementos_tecnicos_tango_I">elementos técnicos tango I</option>
				<option value="elementos_tecnicos_tango_II">elementos técnicos tango II</option>
				<option value="elementos_tecnicos_tango_III">elementos técnicos tango III</option>
				<option value="flauta_tango">flauta tango</option>
				<option value="guitarra_tango">guitarra tango</option>
				<option value="historia_tango_I">historia tango I</option>
				<option value="historia_tango_II">historia tango II</option>
				<option value="orquesta_tango">orquesta tango</option>
				<option value="piano_tango">piano tango</option>
				<option value="practica_grupal_tango_I">práctica grupal tango I</option>
				<option value="practica_grupal_tango_II">práctica grupal tango II</option>
				<option value="practica_grupal_tango_III">práctica grupal tango III</option>
				<option value="saxo_tango">saxo tango</option>
				<option value="taller_de_cuarteto_de_guitarras">taller de cuarteto de guitarras</option>
				<option value="taller_de_improvisacion_tango">taller de improvisacion tango</option>
				<option value="taller_percusion_tango">taller percusión tango</option>
				<option value="violin_tango">violín tango</option>


					</select>
					<div>
						<label>Condicion</label>
						<select name="condicion'.$i.'" >
							<option value="">-SELECCIONE-</option>
							<option value="libre">Libre</option>
							<option value="previo">Previo</option>
							<option value="regular">Regular</option>
						</select>
					</div>
					<div>
						<label>Profesor</label>
						<input type="text" name="profesor'.$i.'" class="tooltip" title="Nombre del profesor si se inscribe como previo o regular"/>
					</div>
				<div>
						<label>Nivel</label>
						<select name="nivel'.$i.'" class="nivelSelect">
							<option value=""'.estadoInput($_POST["nivel"],0,"select").'>-SELECCIONE-</option>
							<optgroup label="Formación Básica">
								<option value="Preparatorio formación Básica"'.estadoInput($_POST["nivel"],1,"select").'>Preparatorio formación Básica</option>
								<option value="Nivel I"'.estadoInput($_POST["nivel"],2,"select").'>Nivel I </option>
								<option value="Nivel II"'.estadoInput($_POST["nivel"],3,"select").'>Nivel II </option>
								<option value="Nivel III"'.estadoInput($_POST["nivel"],4,"select").'>Nivel III </option>
							</optgroup>
							<optgroup label="Ciclo Superior">
								<option value="1ero superior"'.estadoInput($_POST["nivel"],5,"select").'>1º año</option>
								<option value="2ndo superior"'.estadoInput($_POST["nivel"],5,"select").'>2º año</option>
								<option value="3ero superior"'.estadoInput($_POST["nivel"],5,"select").'>3º año</option>
								<option value="4to superior"'.estadoInput($_POST["nivel"],5,"select").'>4º año</option>
							</optgroup>

						</select>
				</div>
					</div></div>
						';
					}
					?>
					<input type="submit" value="Enviar" />


				</form>

