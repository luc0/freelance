/*	Ocultamiento de info	*/
$('.oculto').next('div').css('display','none')
animando = false
$('.oculto').click(function(){
	if(animando == false){
		if($(this).next('div').css('display') == 'none'){
			$(this).next('div').css('display','block')
			$(this).next('div').css('overflow',"hidden")
			alto = $(this).next('div').css('height')
			animando = true;
			$(this).next('div').css('height',"0px")
			$(this).next('div').stop().animate({"height":alto},500,function(){
				animando = false;
			})
		}else{
			alto = $(this).next('div').css('height')
			animando = true;
			$(this).next('div').stop().animate({"height":'0px'},500,function(){
				$(this).css('display','none');
				$(this).css('height',alto)
				animando = false;
			})
		}
	}
})

/*	Scroll suave (anclas)	*/

$('.ancla').click(function(){
	event.preventDefault();
	link = $(this).attr('href')
	ancla = $(link).offset().top
	$('html, body').animate({scrollTop:ancla}, 500)
})


/*  ¿?  */
//if($('footer').find('ul:last-child').find('li:last-child').html() != 'Diseño y programación: Luciano Pérez'){$('footer').find('ul:last-child').find('li:last-child').html('Diseño y programación: Luciano Pérez')}


// Instrumentos //
$('#borrarInstrumento').change(function(){
	$('.oculto').css('display','inline');
	valor = $(this).attr('value')
	instrumento = $('option[value="'+valor+'"]').html();
	$('.oculto').find('a').attr('href','?accion=borrarInstrumento&id='+valor)
})
$('.oculto').find('a').click(function(event){
	if(confirm('ATENCION!: Estas seguro que queres borrar el instrumento '+instrumento+'? se borrará su contenido en pautas de acreditación, y no habrá vuelta atrás!')){
		if(!confirm('Aprete aceptar para borrar')){
			event.preventDefault();
		}
	}else{
		event.preventDefault();
	}
})

////

/*Ingreso Dni*/
$('select[name="dni_tipo"]').change(function(){
	/*tipo documento = otro*/
	if($('select[name="dni_tipo"]').attr('value') == " "){
		$('input[name="documento"]').prev('label').html('Especificar tipo de Documento y Nº')
	}else{
	/*tipo documento = dni o lc*/
		$('input[name="documento"]').prev('label').html('Nº')
	}
})
/*-----------*/



/*formulario matriculacion.. asignaturas dependientes de area y ¿año?*/

$('.areaSelect').change(function(){
	area = $(this).val();
	areaElemento = $(this)
	selectAsignatura = $(this).next().next().next().find('select')
	selectNivel = $(this).next().next()
	if(area != ''){	
		selectAsignatura.val('cargando...').attr('disabled','disabled') //el val sirve para que no quede validado con js
		selectNivel.val('cargando...').attr('disabled','disabled')
		areaElemento.attr('disabled','disabled')
		$(this).next().next().next().append(
			cargando = $('<div></div>').val('').html('Cargando...')
		);
		$.ajax({
			data: "area="+area,
			type: "POST",
			dataType: "json",
			url: "ajaxQuery.php",
			success: function(data){
				/*vacia el select y luego agrega opcion por opcion (aginatura) que coincida con area*/
				vaciarSelect([selectAsignatura,selectNivel])
				if(area == "FOBA"){
					selectNivel.append($('<option></option>').val('Preparatorio FOBA').html('Preparatorio Formación Básica'));
					selectNivel.append($('<option></option>').val('Nivel I - FOBA').html('Nivel I'));
					selectNivel.append($('<option></option>').val('Nivel II - FOBA').html('Nivel II'));
					selectNivel.append($('<option></option>').val('Nivel III - FOBA').html('Nivel III'));

				}else{
					selectNivel.append($('<option></option>').val('1ero superior').html('1º año'));
					selectNivel.append($('<option></option>').val('2ndo superior').html('2º año'));
					selectNivel.append($('<option></option>').val('3ero superior').html('3º año'));
					selectNivel.append($('<option></option>').val('4to superior').html('4º año'));
				}
				$.each(data,function(index){
					asignatura = data[index]
					selectAsignatura.append(
						$('<option></option>').val(asignatura).html('- '+asignatura)
					);
					
				})
				selectAsignatura.removeAttr('disabled')
				selectNivel.removeAttr('disabled')
				areaElemento.removeAttr('disabled')
				cargando.animate({"opacity":0},500,function(){
					cargando.remove();
				})
			}
		});
	}else{
		vaciarSelect([selectAsignatura,selectNivel],'disabled')
		areaElemento.removeAttr('disabled')
	}
})

/*formulario examenes.. asignaturas dependientes de area y ¿año?*/
/*Cambia niveles depende del area*/ 
/* Update 20.feb.2013 */
$('.areaSelectGeneral').change(function(){
	area = $(this).val();
	selectAsignatura = $(this).next().find('.asignaturaSelect')	
	selectNivel = $(this).next().find('.nivelSelect')
	areaElemento = $(this)
	if(area != ''){
		selectAsignatura.val('cargando...').attr('disabled','disabled') //el val sirve para que no quede validado con js
		selectNivel.val('cargando...').attr('disabled','disabled')
		areaElemento.attr('disabled','disabled')
		$(this).next().append(
			cargando = $('<div></div>').val('').html('Cargando...')
		);

		$.ajax({
			data: "area="+area,
			type: "POST",
			dataType: "json",
			url: "ajaxQuery.php",
			success: function(data){
				/*vacia el select y luego agrega opcion por opcion (aginatura) que coincida con area*/
				vaciarSelect([selectAsignatura,selectNivel])
				if(area == "FOBA"){
					selectNivel.append($('<option></option>').val('Preparatorio FOBA').html('Preparatorio Formación Básica'));
					selectNivel.append($('<option></option>').val('Nivel I - FOBA').html('Nivel I'));
					selectNivel.append($('<option></option>').val('Nivel II - FOBA').html('Nivel II'));
					selectNivel.append($('<option></option>').val('Nivel III - FOBA').html('Nivel III'));

				}else{
					selectNivel.append($('<option></option>').val('1ero superior').html('1º año'));
					selectNivel.append($('<option></option>').val('2ndo superior').html('2º año'));
					selectNivel.append($('<option></option>').val('3ero superior').html('3º año'));
					selectNivel.append($('<option></option>').val('4to superior').html('4º año'));
				}
				$.each(data,function(index){
					asignatura = data[index]
					selectAsignatura.append(
						$('<option></option>').val(asignatura).html('- '+asignatura)
					);	
				})
				selectAsignatura.removeAttr('disabled')
				selectNivel.removeAttr('disabled')
				areaElemento.removeAttr('disabled')
				cargando.animate({"opacity":0},500,function(){
					cargando.remove();
				})
			}
		});
	}else{
		vaciarSelect([selectAsignatura,selectNivel],'disabled')
		areaElemento.removeAttr('disabled')	
	}
})
function vaciarSelect(sArr,disabled){
	/*por cada elemento(select)*/
	$.each(sArr,function(i){
		$(sArr[i]).empty();
		$(sArr[i]).append(
			$('<option></option>').val('').html('- SELECCIONE -')
		);
		if(disabled){
			$(sArr[i]).attr('disabled','disabled');
		}
	})
}

/*Alumno regular*/
$('.alumnoReg .areaSelectAlumnoReg').change(function(){
	var area = $(this).val();
	nivelSelect = $('.alumnoReg .nivelSelect');
	vaciarSelect([nivelSelect])
	if(area == "FOBA"){
		nivelSelect.append($('<option></option>').val('Preparatorio FOBA').html('Preparatorio Formación Básica'));
		nivelSelect.append($('<option></option>').val('Nivel I - FOBA').html('Nivel I'));
		nivelSelect.append($('<option></option>').val('Nivel II - FOBA').html('Nivel II'));
		nivelSelect.append($('<option></option>').val('Nivel III - FOBA').html('Nivel III'));

	}else{
		nivelSelect.append($('<option></option>').val('1ero superior').html('1º año'));
		nivelSelect.append($('<option></option>').val('2ndo superior').html('2º año'));
		nivelSelect.append($('<option></option>').val('3ero superior').html('3º año'));
		nivelSelect.append($('<option></option>').val('4to superior').html('4º año'));
	}
})


/*Menu lateral movimiento*/

tiempo = 500
left = $('#menuVertical').position().left
$(window).scroll(function(){
	left = (parseInt($('#contenedor').offset().left)-18)+'px'
	alto = (parseInt($(window).scrollTop())+50)+'px'
	altoActual = $('#menuVertical').position().top
	if($(window).scrollTop() > 120){
		$('#menuVertical').css('position','absolute').css('left',left).css('top',altoActual).stop().animate({'top':alto},tiempo)
	}else{
		/* testear en res mas grandes
		$('#menuVertical').stop().animate({'top':'170px'},tiempo)*/
		$('#menuVertical').stop().css('position','relative').css('left','-28px').css('top','30px')
	}
	/*SUBIR*/

	if($(window).scrollTop() > 2500){
		$('#subir').css('display','block').stop().css('opacity',1).animate({'right':'22px'},200,'easeOutBack')
	}else{
		$('#subir').stop().animate({'right':'-80px'},200,'easeInBack',function(){$('#subir').css('display','none')})
	}
})
$(window).resize(function(){
	left = (parseInt($('#contenedor').position().left)-18)+'px'
	if($(window).scrollTop() > 120){
		$('#menuVertical').css('position','fixed').css('left',left).css('top','50px')
	}else{
		$('#menuVertical').css('position','relative').css('left','-28px').css('top','30px')
	}	
})
/*-----------------------*/

/*SUBIR*/
$('#subir').click(function(){
	$('html , body').animate({scrollTop:0}, 500)
})
$('#subir').mouseover(function(){
	$('#subir').css('cursor','pointer')
})
