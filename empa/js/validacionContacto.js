// texto , numero , tel , email , fecha

//	campos	|  tipo
	nombre = 'texto'
	apellido = 'texto'
	email = 'email'
	destino = '-'
	mensaje = '*'


//	(1) estatico | (2) animado
	mostrarError = 2;

//	color normal del label (sirve si le pongo color='no')
	colorPredeterminado = 'black'
//	color error del label PONER 'no' si no lo quiero
	colorError = 'red'
	colorCorrecto = '#9CCC7D';
//	modificar los bordes de los input? (si:1 | no:0)
	modificarBordes = 0

//	validar: (1) submit | (2) porInput | (3) tiempoReal
	modo = 3

//------------------------------------------//

switch(modo){
	case 1:
		$('input[type="submit"]').click(function(){
			validar(modo);
		})
		break;
	case 2:
		$('input[type="submit"]').attr('disabled','disabled')
		$(':input').change(function(){
			validar(modo);
		})
		break;
	case 3:
		$('input[type="submit"]').attr('disabled','disabled')
		$(':input').keyup(function(){
			validar(2); //mismo modo de validar que el 2
		})
		break;

}

function validar(modo){
	valido = true
	inputs = $(':input')

	switch(modo){
		case 1:
			$('form').submit(function(){
				valido = analizar(inputs)
				if(!valido) return false;
				$('input[type="submit"]').attr('value','Enviando...');
				//$(':input').attr('disabled','disabled');
				return true;
			})
			break;
		case 2:
			valido = analizar(inputs)
			if(valido){
				$(':input').removeAttr('disabled');
				$('form').submit(function(){
					$('input[type="submit"]').attr('value','Enviando...');
					//$(':input').attr('disabled','disabled');
					return true;
				})
			}else{
				$('input[type="submit"]').attr('disabled','disabled')
			}

			break;
	}
}


function pintarLabel(color,borde){

	if(color == 'no'){
		switch(mostrarError){
			case 1:
				$(":input[name="+label+"]").prev('label').css("color", colorPredeterminado)
				break;
			case 2:
				$(":input[name="+label+"]").prev('label').stop().animate({color:colorPredeterminado},500)
				break;
		}

	}else{
		switch(mostrarError){
			case 1:
				$(":input[name="+label+"]").prev('label').css("color", color)
				break;
			case 2:
				$(":input[name="+label+"]").prev('label').stop().animate({color:color},500)
				break;
		}
		if(modificarBordes){
			switch(borde){
				case "sinBorde":
					bordeInput = $(":input[name="+label+"]").css('border')
					$(":input[name="+label+"]").css('border','none')
					break;
				case "conBorde":
					$(":input[name="+label+"]").css('border','2px inset')
					$(":input[name="+label+"]").css('borderColor','') /*sin esto creo que IE se bugea*/
					break;
			}
		}
	}
}


function analizar(inputs){
	validacion = true;
	for(i=0;i<inputs.length;i++){
		label = inputs[i].name
		switch(eval(inputs[i].name)){
			case '*':
				if(inputs[i].value != ''){
					pintarLabel(colorCorrecto,'sinBorde')
				}else{
					pintarLabel(colorError,'conBorde')
					validacion = false
				}
				break;
			case 'texto':
				//inputs[i].value = inputs[i].value.split(' ').join('')
				if(inputs[i].value.match('^[a-zA-Z]+$')){
					pintarLabel(colorCorrecto,'sinBorde')
				}else{
					pintarLabel(colorError,'conBorde')
					validacion = false
				}
				break;
			case 'numero':
				inputs[i].value = inputs[i].value.split(' ').join('')
				if(inputs[i].value.match('^[0-9]+$')){
					pintarLabel(colorCorrecto,'sinBorde')
				}else{
					pintarLabel(colorError,'conBorde')
					validacion = false
				}
				break;
			case 'tel':
				inputs[i].value = inputs[i].value.split('-').join('')
				if(inputs[i].value.match('^[0-9]{6,16}$')){
					pintarLabel(colorCorrecto,'sinBorde')
				}else{
					pintarLabel(colorError,'conBorde')
					validacion = false
				}
				break;
			case 'email':
				inputs[i].value = inputs[i].value.split(' ').join('')
				if(inputs[i].value.match(/^(.+\@.+\..+)$/)){
					pintarLabel(colorCorrecto,'sinBorde')
				}else{
					pintarLabel(colorError,'conBorde')
					validacion = false
				}
				break;
			case 'fecha':
				inputs[i].value = inputs[i].value.split('-').join('/')
				inputs[i].value = inputs[i].value.split('.').join('/')
				inputs[i].value = inputs[i].value.split(' ').join('')


				valoresFecha = [];
				valoresFecha = inputs[i].value.split('/')
				if(valoresFecha[1] < 8){
					 diamax = 30 + valoresFecha[1] % 2
				}else{
					diamax = 31 - valoresFecha[1] % 2
				}
				if(valoresFecha[1] == 2) diamax = 28 + (((parseInt(valoresFecha[2])+1) % 4 ) == 0)
				valoresCorrectos = valoresFecha[0] <= diamax && valoresFecha[1] < 13 && valoresFecha[2] < 2050 && valoresFecha[2] > 1900
				 
				if(inputs[i].value.match('^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}$') && valoresCorrectos){
					pintarLabel(colorCorrecto,'sinBorde')
				}else{
					pintarLabel(colorError,'conBorde')
					validacion = false
				}
				break;
		}

	}

	return validacion

}



