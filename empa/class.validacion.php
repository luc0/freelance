<?php

// -- No Modificar -- //

class Validacion{

	/*
	REF:
	valor[0] , valor[1] , son los tipo de datos
	resultado[0] , resultado[1] devuelve true o false segun sea valido el campo o no
	*/

	public function __construct(){
		$argumentos = func_num_args();
		for($i=0;$i<$argumentos;$i++){
			$this->valor[$i] = func_get_arg($i);
		}
	}
	public function campos(){
		$argumentos = func_num_args();
		for($i=0;$i<$argumentos;$i++){
			$getArg = func_get_arg($i);
			$this->resultado[$i] = $this->validar($_POST[$getArg],$this->valor[$i],$getArg);
		}
		return  $this->resultado;
	}
	public function validar($valor,$tipo,$campo){
		switch($tipo){
			case 'letras':
				//acepta letras con signos, la comita y espacios
				if(preg_match('/^[a-zA-ZáéíóúÄËÏÜÖÂÊÎÔÛñÑ\'\s\\\\]+$/',$valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'numeros':
				if(is_numeric($valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'alfanumerico':
				if(preg_match('/^[0-9a-zA-Z\sñáéíóúÁÉÍÓÚÑ\'\.]+$/',$valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'mail':
				if(preg_match('/^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,4}$/i',$valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'fecha':
				// Ej: "10/15/2007";
				if(preg_match('/^\d{1,2}\/|-\d{1,2}\/|-\d{4}$/',$valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'tel':
				// solo numeros | caracteres: 6-16
				if(preg_match('/^[0-9\-]{6,16}$/',$valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'nick':
				// 8-22 caracteres || debe empezar con minuscula o mayuscula || acepta letras numeros y guion
				if(preg_match('#^ [a-z]  [\da-z_] {6,22} [a-z\d] \$#i',$valor)){
					return false;
				}else{
					return $campo;
				}
				break;
			case 'algo':
				//si tiene algo (cualquier cosa)
				if(!empty($valor)){
					return false;
				}else{
					return $campo;
				}
				break;

			case '-':
				//no validar
				return false;
				break;

		}
	}

}

?>
