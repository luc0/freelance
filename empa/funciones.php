<?php
require_once('inyeccion.php');

function limpiarArray($arr)
{
       foreach($arr as $key=> $valor)
      {
          if(empty($valor)) unset($arr[$key]);
      }
      return $arr;
}  

function caracteres_html($texto){
      $texto = htmlentities($texto,ENT_NOQUOTES, 'ISO-8859-1'); // Convertir caracteres especiales a entidades
      $texto = htmlspecialchars_decode($texto,ENT_NOQUOTES); // Dejar <, & y > como estaban
      return $texto;
}


//Para rellenar nuevamente los campos de select y checkbox..
function estadoInput($name,$valor,$input){
	switch($input){
		case 'radio':
			if($name==$valor){return 'checked=checked';}
			break;
		case 'select':
			if($name==$valor){return 'selected=selected';}
			break;
		case 'checkbox':
			if($name==$valor){return 'checked=checked';}
			break;
	}
}
function editor(){
	global $urlActual;
	global $seccionContenido;
	global $seccion;
			?>
			<p>Editor del contenido de la pagina:</p>
			<form method="post" action='<?php echo $urlActual; ?>'>
				<p>
					<textarea id="editor1" name="editor1">
					<?php
						// Inserta contenido de la seccion en el editor //
						//while($datos = mysql_fetch_array($contenido)){
							echo $seccionContenido;//$datos['contenido'];
							unset($seccionContenido);
						//}
					?>
					</textarea>
					<script type="text/javascript">
						CKEDITOR.replace( 'editor1',    {
						toolbar :
						[
							['Undo','Redo'],
							['Format','Image','Flash'],
							['Table'],
							['Bold','Underline', 'Italic', '-', 'NumberedList', 'BulletedList', '-',],
							['Link','Anchor'],
						]
						    }
						);

					</script>
				</p>

				<p class='aclaracion'>No utilizar en lo posible el encabezado 1 (el mas grande), sino usar del encabezado 2 en adelante</p>
				<p class='aclaracion'>Para agregar imagenes, flash, o vinculos como mínimo se debe poner la Direccion URL</p>
				<p>
					<input type='hidden' name='contenido' value='<?php echo $seccion; ?>'>
					<input type="submit" value='Guardar' />
				</p>
			</form>
		<?php
}

function contenido(){
	global $seccion;
	global $conexionAdmin;
	global $bd_admin;
	global $seccionContenido;
	if(empty($seccion)){
		$seccion = 'index';
	}
	mysql_select_db($bd_admin,$conexionAdmin);
	$contenido = mysql_query("SELECT contenido.contenido,DATE_FORMAT(contenido.fecha, '%d/%m/%y a las %h:%m:%s hs.') AS fecha FROM contenido WHERE seccion='$seccion'",$conexionAdmin);
	if(mysql_num_rows($contenido)){
		while($datos = mysql_fetch_array($contenido)){
			$contenidoEntidades = caracteres_html($datos['contenido']); //por alguna razon en algunas tablas estan los acentos en vez de entidades, esto me ahorra de pasarlas a entidades, asi no muestra el signo de pregunta en negro.
			$seccionContenido = $contenidoEntidades;
			echo $seccionContenido;
			if(!empty($_SESSION[md5('adminSesion')])){
				echo '<p class="aclaracion">modificacion: '.$datos['fecha'].'</p>';
			}
		}
	}else{
		include_once('error/404.php');
	}
}

function guardarContenido(){
	global $conexionAdmin;
	global $bd_admin;
	//// GUARDA EN BD LO EDITADO (si se guardo algo) ////
	//(solo si tiene sesion de admin [se logueo] y si guardo el formulario)

	if(!empty($_POST['contenido']) && !empty($_SESSION[md5('adminSesion')])){
		$seccionModificada = $_POST['contenido']; // se la pasa hidden en el form (el form la obtubo antes mediante GET)
		$admin = $_SESSION[md5('adminSesion')];
		$contenido = $_POST['editor1'];
		//reemplazo etiquetas viejas por nuevas (por si se se hace copy paste de un word por ej.)
		$etiquetas = array('</i>','</b>','<i>','<b>');
		$reemplazo = array('</em>','</strong>','<em>','<strong>');
		$contenido = str_replace($etiquetas, $reemplazo, $contenido);
		//
		mysql_select_db($bd_admin,$conexionAdmin);

		$buscarSeccion = mysql_query("SELECT * from contenido WHERE seccion = '$seccionModificada'");
		if(mysql_num_rows($buscarSeccion)){//encontro la seccion
			$guardarBackup = mysql_query("UPDATE contenido SET restaurar = contenido WHERE seccion = '$seccionModificada'",$conexionAdmin);
			$guardarQuery = mysql_query("UPDATE contenido SET contenido = '$contenido', admin = '$admin' WHERE seccion = '$seccionModificada'",$conexionAdmin);
		}else{//no existe la seccion, la crea.
			$guardarQuery = mysql_query("INSERT INTO contenido (seccion,contenido,restaurar,admin) VALUES('$seccionModificada','$contenido','$contenido','$admin')",$conexionAdmin);

		}

		if($guardarQuery){
			echo '<div id="validacionOk">';
				echo 'Se modificó el contenido';
			echo '</div>';
		}else{
			echo '<div id="validacionAlert">';
				echo 'No se pudo guardar el contenido, intente nuevamente.';
			echo '</div>';
		}
	}

}

function enviarMail ($asunto,$remitente,$destinatario,$mensaje){

	$headers = "Content-type: text/html; charset=iso-8859-1 
	From: $remitente
	Return-path: $remitente"; 
	$cuerpo .= "<br/><br/> " . $mensaje; 

	if(mail($destinatario,$asunto,$cuerpo,$headers)){
		return true;

	}else{
		return false;

	}
}

?>
