<form method="post" action="?">
	<h3>Datos Personales.</h3>
	<label>Nombre</label><input type="text" name="nombre" maxlength="40" value="<?php echo $_POST['nombre']; ?>"/>
	<label>Apellido</label><input type="text" name="apellido" maxlength="40" value="<?php echo $_POST['apellido']; ?>"/>
	<div>
		<div><label>Sexo</label></div>
		<label>Masculino</label><input type="radio" name="sexo" value="1" checked <?php echo estadoInput($_POST['sexo'],1,'radio'); ?>/>
		<label>Femenino</label><input type="radio" name="sexo" value="2" <?php echo estadoInput($_POST['sexo'],2,'radio'); ?>/>
	</div>
	<div>
		<div><label>Documento</label></div>
		<label>Tipo</label>
		<select name="dni_tipo" class="chico">
			<option value="dni" <?php echo estadoInput($_POST['dni(tipo)'],'dni','select'); ?>>DNI</option>
			<option value="lc" <?php echo estadoInput($_POST['dni(tipo)'],'lc','select'); ?>>LC</option>
			<option value=" " <?php echo estadoInput($_POST['dni(tipo)'],' ','select'); ?>>Otro..</option>
		</select>
		<div>
			<label>nº</label><input type="text" name="documento" maxlength="45" value="<?php echo $_POST['documento']; ?>"/>
		</div>
	</div>
	<div>
		<div>
			<label>Fecha de nacimiento</label>
		</div>
		<input type="text" class="tooltip chico" name="nacimiento" maxlength="20" value="<?php echo $_POST['nacimiento']; ?>" title="Ej: 17/03/1990"/><!--<span>Ej: 31/01/1985</span>-->
	</div>
	<label>Nacionalidad</label><input type="text" name="nacionalidad" maxlength="30" value="<?php echo $_POST['nacionalidad']; ?>"/>
	<label>Año de Ingreso a la Empa</label><input type="text" class="tooltip" name="ano_ingreso" maxlength="20" value="<?php echo $_POST['ano_ingreso']; ?>" /><!--title="Ej: 2011"-->
	<div>
		<label>Ultimo año aprobado completo</label><input type="text" class="tooltip" name="ultimo_aprobado" maxlength="50" value="<?php echo $_POST['ano_ingreso']; ?>" /><!--title="Ej: 1º F.O.B.A"-->
		<em>"Aprobado" significa la aprobación de las cursadas, independientemente de los exámenes finales</em>
	</div>
	<label>Instrumento</label>
	<select name="instrumento">
		<?php
			mysql_select_db($bd_ingresantes,$conexion);
			$resp = mysql_query("SELECT * from instrumentos WHERE activo = 1 ORDER BY valor",$conexion);
			echo "<option value=''>-SELECCIONE-</option>";
			while($datos = mysql_fetch_array($resp)){
				echo '<option value="'.$datos['id'].'" '.estadoInput($_POST['instrumento'],1,'select').'>'.$datos['valor'].'</option>';
			}
		?>

	</select>

	<div>
		<label>Carrera</label>
		<select name="carrera">
			<option value="foba" <?php echo estadoInput($_POST['dni(foba)'],'foba','select'); ?>>FORMACIÓN BÁSICA</option>
			<option value="timp" <?php echo estadoInput($_POST['dni(timp)'],'timp','select'); ?>>TECNICATURA EN MÚSICA POPULAR</option>
			<option value="pimp" <?php echo estadoInput($_POST['dni(pimp)'],'pimp','select'); ?>>PROFESORADO DE INSTRUMENTO</option>
			<option value="fodo" <?php echo estadoInput($_POST['dni(fodo)'],'fodo','select'); ?>>FORMACIÓN DOCENTE</option>
		</select>
	</div>

	<label>Email</label><input type="text" class="tooltip" name="email" maxlength="50" value="<?php echo $_POST['email']; ?>" title="Ingresa un mail válido ya que es donde se te enviará el link para imprimir el comprobante."/>
		<label>Domicilio</label>
		<div>
			<label>Calle</label><input type="text" name="calle_nombre" maxlength="35" value="<?php echo $_POST['calle_nombre']; ?>"/><label>nº</label><input type="text" name="calle_numero" class="chico" maxlength="35" value="<?php echo $_POST['calle_numero']; ?>"/>
			<label>Localidad</label><input type="text" name="localidad" maxlength="50" value="<?php echo $_POST['localidad']; ?>"/>
		</div>
		<div>
			<label>Teléfono</label><input type="text" name="telefono" maxlength="30" value="<?php echo $_POST['telefono']; ?>"/>
			<label>Celular/Mobile</label><input type="text" name="celular" maxlength="30" value="<?php echo $_POST['celular']; ?>"/>
		</div>

	<p>La recepción de la presente No implica asignación de horarios. La Matriculación On Line significa anotarse en las asignaturas que van a cursar. La asignación de horarios se realizará en la segunda etapa.</p>
	<input type="submit" value="Enviar" />


</form>
